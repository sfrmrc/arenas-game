package it.rome.arenas;

import it.rome.arena.client.template.BulletTemplate;
import it.rome.arena.client.template.InterpolatedTemplate;
import it.rome.arena.client.template.LocalPlayerTemplate;
import it.rome.arena.client.template.PlayerTemplate;
import it.rome.arena.client.template.RemotePlayerTemplate;
import it.rome.arena.shared.Group;
import it.rome.arena.shared.Layers;
import it.rome.arena.shared.Tag;
import it.rome.arenas.template.AimTemplate;
import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.impl.PlayerShared;
import it.rome.game.shared.attribute.impl.SpellShared;
import it.rome.game.shared.template.TagTemplate;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.Parameters;
import it.rome.game.template.TextTemplate;

import java.util.Collection;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.TagManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ClientGameObjectFactory {

  static World world;
  static com.badlogic.gdx.physics.box2d.World physicWorld;
  static TagManager tagManager;
  static EntityFactory entityFactory;
  static BodyBuilder bodyBuilder;
  static PlayerTemplate playerTemplate;
  static RemotePlayerTemplate remotePlayerTemplate;
  static LocalPlayerTemplate localPlayerTemplate;
  static InterpolatedTemplate interpolatedTemplate;
  static TagTemplate tagTemplate;
  static TextTemplate textTemplate;
  static BulletTemplate bulletTemplate;
  static AimTemplate aimTemplate;

  private ClientGameObjectFactory() {}

  public static void init(World _world, com.badlogic.gdx.physics.box2d.World _physicWorld) {
    world = _world;
    physicWorld = _physicWorld;
    tagManager = world.getManager(TagManager.class);
    entityFactory = new EntityFactory(world);
    bodyBuilder = new BodyBuilder(physicWorld);
    playerTemplate = new PlayerTemplate(bodyBuilder);
    bulletTemplate = new BulletTemplate(bodyBuilder);
    tagTemplate = new TagTemplate();
    textTemplate = new TextTemplate();
    remotePlayerTemplate = new RemotePlayerTemplate();
    localPlayerTemplate = new LocalPlayerTemplate();
    interpolatedTemplate = new InterpolatedTemplate();
    aimTemplate = new AimTemplate();
  }

  public static void createAim() {
    entityFactory.instantiate(
        new Parameters().put("spatial", new SpatialImpl(0, 0)).put("tag", Tag.CURSOR), aimTemplate,
        tagTemplate);
  }

  public static void createPlayer(PlayerShared shared, Collection<Spell> spellBook) {
    entityFactory.instantiate(
        new Parameters().put("spatial", new SpatialImpl(320f, 240f, 30f, 30f))
            .put("networkId", shared.getNetworkId()).put("tag", shared.getNetworkId())
            .put("stats", shared.getStats()).put("spellBook", spellBook)
            .put("group", new String[] {Group.CASTER, Tag.PLAYER}), playerTemplate,
        localPlayerTemplate, tagTemplate);
    Gdx.app.log("DEBUG", "Client: created object " + shared.getNetworkId());
  }

  public static void createFadingText(String message) {
    entityFactory.instantiate(
        textTemplate,
        new Parameters()
            .put("spatial",
                new SpatialImpl(Gdx.graphics.getWidth() * .5f, Gdx.graphics.getHeight() * .5f))
            .put("text", message).put("fading", 2f).put("color", Color.RED)
            .put("layer", Layers.MESSAGE));
  }

  public static void createFadingText(String message, float x, float y) {
    entityFactory.instantiate(textTemplate, new Parameters().put("spatial", new SpatialImpl(x, y))
        .put("text", message).put("fading", 2f).put("color", Color.RED)
        .put("layer", Layers.MESSAGE));
  }

  public static void createRemotePlayer(PlayerShared shared) {
    entityFactory.instantiate(
        new Parameters().put("spatial", new SpatialImpl(320f, 240f, 30f, 30f))
            .put("stats", shared.getStats()).put("group", Group.ENEMY)
            .put("tag", shared.getNetworkId()).put("networkId", shared.getNetworkId()),
        playerTemplate, interpolatedTemplate, tagTemplate, remotePlayerTemplate);
    Gdx.app.log("DEBUG", "Client: created object " + shared.getNetworkId());
  }

  public static void createObject(SpellShared shared) {
    Entity caster = tagManager.getEntity(shared.getCasterId());
    entityFactory.instantiate(
        new Parameters().put("x0", shared.getSpatial().getX())
            .put("y0", shared.getSpatial().getY()).put("commandId", shared.getCommandId())
            .put("casterRef", new EntityRef(caster)).put("tag", shared.getNetworkId()),
        bulletTemplate, tagTemplate, interpolatedTemplate);
    Gdx.app.log("DEBUG", "Client: created object " + shared.getNetworkId());
  }

}
