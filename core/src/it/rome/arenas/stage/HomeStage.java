package it.rome.arenas.stage;

import it.rome.arenas.ArenaResourceManager;
import it.rome.arenas.screen.HomeScreen;
import it.rome.game.shared.attribute.Stats;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.uwsoft.editor.renderer.Overlap2DStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.script.SimpleButtonScript;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class HomeStage extends Overlap2DStage {

  static final String CHARACTER_NAME = "characterName";
  static final String PLAYER_PLACEHOLDER = "playerPlaceholder";
  static final String BEGIN_BUTTON = "beginButton";

  private final HomeScreen screen;
  private CompositeItem root;

  public HomeStage(HomeScreen screen) {
    super();
    Gdx.input.setInputProcessor(this);
    this.screen = screen;
    ArenaResourceManager rm = new ArenaResourceManager();
    rm.initAllResources();
    initSceneLoader(rm);
    initHome();
  }

  public void show(Stats[] stats) {
    CompositeItem playerPlaceholder = root.getCompositeById(PLAYER_PLACEHOLDER);
    playerPlaceholder.clear();
    for (Stats s : stats) {
      CompositeItem player = sceneLoader.getLibraryAsActor(s.getRace().toString());
      playerPlaceholder.addActor(player);
    }
  }

  public void setUser(String username) {
    Label label = root.getLabelById(CHARACTER_NAME);
    label.setText(username);
    Gdx.app.log("INFO", "Client: my user " + username);
  }

  public void showQueue() {
    Gdx.app.log("INFO", "Client: begin arena queue ...");
  }

  private void initHome() {
    clear();
    sceneLoader.loadScene("HomeScene");
    root = sceneLoader.getRoot();
    SimpleButtonScript beginButton =
        SimpleButtonScript.selfInit(root.getCompositeById(BEGIN_BUTTON));
    beginButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        screen.arenaQueue();
      }
    });
    addActor(root);
  }


}
