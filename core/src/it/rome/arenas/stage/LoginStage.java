package it.rome.arenas.stage;

import it.rome.arenas.ArenaResourceManager;
import it.rome.arenas.screen.LoginScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.uwsoft.editor.renderer.Overlap2DStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.LabelItem;
import com.uwsoft.editor.renderer.actor.TextBoxItem;
import com.uwsoft.editor.renderer.script.SimpleButtonScript;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class LoginStage extends Overlap2DStage {

  static final String LOGIN_FORM = "loginForm";
  static final String LOGIN_BUTTON = "loginButton";
  static final String CANCEL_BUTTON = "cancelButton";

  static final String USERNAME_TEXT = "username";
  static final String PASSWORD_TEXT = "password";

  static final String ERROR_BOX = "errorBox";
  static final String ERROR_MSG = "errorMsg";

  final LoginScreen screen;
  CompositeItem root;
  CompositeItem form;
  TextBoxItem username;
  TextBoxItem password;

  public LoginStage(LoginScreen screen) {
    super();
    Gdx.input.setInputProcessor(this);
    this.screen = screen;
    ArenaResourceManager rm = new ArenaResourceManager();
    rm.initAllResources();
    initSceneLoader(rm);
    initLoginForm();
  }

  private void initLoginForm() {
    clear();
    sceneLoader.loadScene("MainScene");
    root = sceneLoader.getRoot();
    form = root.getCompositeById(LOGIN_FORM);
    username = form.getTextBoxById(USERNAME_TEXT);
    password = form.getTextBoxById(PASSWORD_TEXT);
    SimpleButtonScript loginButton =
        SimpleButtonScript.selfInit(root.getCompositeById(LOGIN_BUTTON));
    loginButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        screen.login(username.getText(), password.getText());
      }
    });
    addActor(root);
  }

  public void showErrorMessage(String message) {
    final CompositeItem errorBox = sceneLoader.getLibraryAsActor(ERROR_BOX);
    LabelItem errorMsg = (LabelItem) errorBox.getItemById(ERROR_MSG);
    errorMsg.setText(message);
    SimpleButtonScript cancelButton = SimpleButtonScript.selfInit(errorBox);
    cancelButton.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        errorBox.remove();
      }
    });
    addActor(errorBox);
  }
}
