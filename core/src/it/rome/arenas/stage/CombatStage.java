package it.rome.arenas.stage;

import it.rome.arenas.ArenaResourceManager;
import it.rome.arenas.ui.ActionBarScript;
import it.rome.arenas.ui.ActionBarScript.SpellClickListener;
import it.rome.arenas.ui.PortraitScript;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;

import java.util.Collection;

import com.badlogic.gdx.Gdx;
import com.uwsoft.editor.renderer.Overlap2DStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class CombatStage extends Overlap2DStage {

  static final String MY_PORTRAIT = "myPortrait";
  static final String ACTION_BAR = "actionBarPlaceholder";

  final CompositeItem root;
  SpellClickListener listener;

  public CombatStage() {
    ArenaResourceManager rm = new ArenaResourceManager();
    rm.initAllResources();
    initSceneLoader(rm);
    sceneLoader.loadScene("PlayScene");
    root = sceneLoader.getRoot();
    addActor(root);
    Gdx.input.setInputProcessor(this);
  }

  public void addPlayer(String name, Stats stats, Collection<Spell> spellBook,
      SpellClickListener listener) {
    CompositeItem portrait = root.getCompositeById(MY_PORTRAIT);
    portrait.addScript(new PortraitScript(stats));
    CompositeItem actionBar = root.getCompositeById(ACTION_BAR);
    actionBar.addScript(new ActionBarScript(spellBook, sceneLoader, listener));
  }

}
