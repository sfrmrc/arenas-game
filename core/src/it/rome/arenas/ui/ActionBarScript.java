package it.rome.arenas.ui;

import it.rome.game.shared.attribute.Spell;

import java.util.Collection;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.uwsoft.editor.renderer.SceneLoader;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.ImageItem;
import com.uwsoft.editor.renderer.data.SimpleImageVO;
import com.uwsoft.editor.renderer.script.IScript;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ActionBarScript implements IScript {

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public interface SpellClickListener {
    public void onSpellClicked(Spell spell);
  }

  static final String SPELL_BUTTON = "spellButton";
  static final String SPELL_IMAGE = "icon";

  final Collection<Spell> spellBook;
  final SceneLoader sceneLoader;
  final SpellClickListener listener;
  CompositeItem item;

  public ActionBarScript(Collection<Spell> spellBook, SceneLoader sceneLoader,
      SpellClickListener listener) {
    this.spellBook = spellBook;
    this.sceneLoader = sceneLoader;
    this.listener = listener;
  }

  @Override
  public void init(CompositeItem item) {
    this.item = item;

    float offset = 0;
    for (final Spell spell : spellBook) {
      CompositeItem i = sceneLoader.getLibraryAsActor(SPELL_BUTTON);
      i.setX(offset);
      SpellScript script = new SpellScript(spell, SPELL_IMAGE + spell.getId());
      i.addScript(script);
      item.addItem(i);
      offset += i.getWidth();
    }

  }

  @Override
  public void act(float delta) {}

  @Override
  public void dispose() {}

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class SpellScript implements IScript {

    final Spell spell;
    final String iconName;
    CompositeItem item;

    public SpellScript(Spell spell, String iconName) {
      super();
      this.spell = spell;
      this.iconName = iconName;
    }

    @Override
    public void init(CompositeItem item) {
      this.item = item;
      SimpleImageVO simpleImageVO = new SimpleImageVO();
      simpleImageVO.imageName = iconName;
      simpleImageVO.scaleX = this.item.getDataVO().scaleX;
      simpleImageVO.scaleY = this.item.getDataVO().scaleY;
      ImageItem imageItem = new ImageItem(simpleImageVO, sceneLoader.essentials);
      this.item.addItem(imageItem);
      this.item.addListener(new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
          if (listener != null) {
            listener.onSpellClicked(spell);
          }
        }
      });
    }

    @Override
    public void act(float delta) {}

    @Override
    public void dispose() {}

  }

}
