package it.rome.arenas.ui;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.Image9patchItem;
import com.uwsoft.editor.renderer.actor.ImageItem;
import com.uwsoft.editor.renderer.script.IScript;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ProgressBarScript implements IScript {

  static final String BACKGROUND = "bg";
  static final String PROGRESS = "prgPatch";
  static final String LABEL = "label";
  static final String OVER = "over";

  CompositeItem item;

  ImageItem fill;

  float currentDesiredScale = 0f;
  float currentScale = 0f;
  float speedTime = 0.1f;
  float scaleMoveSpeed = 5f;

  float maxWidth;
  boolean resizing;
  float defaultWidth;
  Image9patchItem bg;
  float defaultBgWidth;
  ImageItem over;
  float defaultOverWidth;

  @Override
  public void init(CompositeItem item) {
    this.item = item;

    fill = item.getImageById(PROGRESS);
    maxWidth = fill.getWidth() * fill.getScaleX();
    defaultWidth = maxWidth;

    try {
      bg = item.getNinePatchById(BACKGROUND);
      defaultBgWidth = bg.getWidth() * bg.getScaleX();
      over = item.getImageById(OVER);
      defaultOverWidth = over.getWidth() * over.getScaleX();
    } catch (Exception e) {}

  }

  @Override
  public void act(float delta) {
    if (!resizing) {
      if (currentScale != currentDesiredScale) {
        float multiplier = 1;
        if (currentScale > currentDesiredScale) multiplier = -1;

        if ((currentScale < currentDesiredScale && currentScale + multiplier * scaleMoveSpeed
            * delta > currentDesiredScale)
            || (currentScale > currentDesiredScale && currentScale + multiplier * scaleMoveSpeed
                * delta < currentDesiredScale) || currentScale == currentDesiredScale)
          currentScale = currentDesiredScale;
        else {
          currentScale += multiplier * scaleMoveSpeed * delta;
        }

      }
      if (Math.abs(currentScale) < 0.001f) currentScale = 0;

      if (maxWidth * currentScale < fill.getMinWidth()) {
        fill.setVisible(false);
      } else {
        fill.setVisible(true);
      }
      if (maxWidth * currentScale < fill.getMinWidth()) {
        fill.setWidth(fill.getMinWidth());
      } else {
        fill.setWidth(maxWidth * currentScale);
      }
    }
  }

  public void setPercent(float percent, boolean instant) {
    if (instant) {
      currentDesiredScale = currentScale = percent / 100f;
      return;
    }

    currentDesiredScale = percent / 100f;

    float diff = Math.abs(currentDesiredScale - currentScale);
    scaleMoveSpeed = diff / speedTime;
  }

  public void setPercent(float percent) {
    setPercent(percent, false);
  }

  @Override
  public void dispose() {}

  public void setLabel(String text) {
    try {
      Label label = item.getLabelById(LABEL);
      label.setText(text);
    } catch (Exception e) {}
  }

  public void resize(float ratio, float duration) {
    resizing = true;

    float center = bg.getX() + bg.getWidth() / 2;
    float padding = fill.getX() - bg.getX() - bg.getMinWidth();

    final float bgWidth =
        (bg.getWidth() - bg.getMinWidth() - padding * 2) * ratio + bg.getMinWidth() + padding * 2;
    // float bgHeight = bg.getHeight();
    float fillWidth =
        (fill.getWidth() - fill.getMinWidth() - padding * 2) * ratio + fill.getMinWidth() + padding
            * 2;
    // float fillHeight = fill.getHeight();
    float overwidth = over.getWidth() * over.getScaleX() * ratio;
    // float overHeight = over.getHeight();

    float newBgX = center - bgWidth / 2;
    float newBgY = bg.getY();
    float newFillX = center - fillWidth / 2;
    float newFillY = fill.getY();
    float newOverX = center - overwidth / 2;
    float newOverY = over.getY();

    // ResizeNinePatchAction resizeActionFill = Actions.action(ResizeNinePatchAction.class)
    // resuzeActionFill.setToSize(fillWidth, fillHeight);
    // resizeActionFill.setDuration(duration);

    fill.clearActions();
    fill.addAction(Actions.parallel(Actions.moveTo(newFillX, newFillY, duration), Actions.sequence(/*
                                                                                                    * resizeActionFill
                                                                                                    * ,
                                                                                                    */
    Actions.run(new Runnable() {
      @Override
      public void run() {
        maxWidth = fill.getWidth() * fill.getScaleX();
        resizing = false;
      }
    }))));

    over.clearActions();
    over.addAction(Actions.parallel(Actions.moveTo(newOverX, newOverY, duration),
        Actions.sequence(Actions.scaleTo(ratio * over.getScaleX(), over.getScaleY() * ratio))));

    // ResizeNinePatchAction resizeActionBg = Actions.action(ResizeNinePatchAction.class)
    // resizeActionBg.setToSize(bgWidth, bgHeight);
    // resizeActionBg.setDuration(duration);
    bg.clearActions();
    bg.addAction(Actions.parallel(Actions.moveTo(newBgX, newBgY, duration)/*
                                                                           * ,Actions.sequence(
                                                                           * resizeActionBg)
                                                                           */));
  }

  public void resetSize() {
    float ratio = defaultWidth / maxWidth;
    resize(ratio, 0.01f);
  }

  public CompositeItem getItem() {
    return item;
  }

}
