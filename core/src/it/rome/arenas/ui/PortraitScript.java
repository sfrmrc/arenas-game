package it.rome.arenas.ui;

import it.rome.game.shared.attribute.Stats;

import com.badlogic.gdx.Gdx;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.script.IScript;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class PortraitScript implements IScript {

  static final String HEALT_BAR = "healthBar";
  static final String MANA_BAR = "manaBar";

  final Stats stats;

  CompositeItem item;
  ProgressBarScript healtBar;
  ProgressBarScript manaBar;
  int life;
  int mana;

  public PortraitScript(Stats stats) {
    this.stats = stats;
  }

  @Override
  public void init(CompositeItem item) {
    this.item = item;
    life = stats.getHealth().getMaxLife();
    mana = stats.getPoints().getMaxPoints();
    healtBar = new ProgressBarScript();
    manaBar = new ProgressBarScript();
    try {
      item.getCompositeById(HEALT_BAR).addScript(healtBar);
      healtBar.setLabel(String.valueOf(life + " HP"));
    } catch (Exception e) {
      Gdx.app.log("ERROR", "Client: " + HEALT_BAR + " component not found");
    }
    try {
      item.getCompositeById(MANA_BAR).addScript(manaBar);
      manaBar.setLabel(String.valueOf(mana));
    } catch (Exception e) {
      Gdx.app.log("ERROR", "Client: " + MANA_BAR + " component not found");
    }
  }

  @Override
  public void act(float delta) {
    if (healtBar != null) {
      float lifePercent = stats.getHealth().getLife() * 100 / life;
      healtBar.setLabel(lifePercent + " %");
      healtBar.setPercent(lifePercent);
    }
    if (manaBar != null) {
      float manaPercent = stats.getPoints().getPoints() * 100 / mana;
      manaBar.setLabel(manaPercent + " %");
      manaBar.setPercent(manaPercent);
    }
  }

  @Override
  public void dispose() {}

  public CompositeItem getItem() {
    return item;
  }

}
