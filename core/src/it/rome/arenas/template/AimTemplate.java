package it.rome.arenas.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.CursorComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class AimTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spatial spatial = parameters.get("spatial");
    SpatialImpl initial = new SpatialImpl(spatial);
    entityEdit.add(new SpatialComponent(initial));
    entityEdit.add(new CursorComponent(initial));
  }

}
