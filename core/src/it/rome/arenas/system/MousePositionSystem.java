package it.rome.arenas.system;

import it.rome.game.attribute.Spatial;
import it.rome.game.component.CursorComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
@Wire
public class MousePositionSystem extends EntityProcessingSystem {

  ComponentMapper<CursorComponent> cm;

  Vector2 position = new Vector2();

  @SuppressWarnings("unchecked")
  public MousePositionSystem() {
    super(Aspect.getAspectForAll(CursorComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    Spatial spatial = cm.get(entity).getSpatial();
    spatial.setPosition(Gdx.input.getX(), Gdx.input.getY());
  }

}
