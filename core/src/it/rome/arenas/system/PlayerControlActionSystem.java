package it.rome.arenas.system;

import it.rome.arena.client.ArenaClient;
import it.rome.arena.shared.Group;
import it.rome.arena.shared.Tag;
import it.rome.arena.shared.listener.packet.CreateCommandEntityPacket;
import it.rome.game.component.CursorComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.shared.Channel;
import it.rome.game.shared.component.WeaponComponent;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Gdx;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
@Wire
public class PlayerControlActionSystem extends VoidEntitySystem {

  ComponentMapper<CursorComponent> cm;
  ComponentMapper<WeaponComponent> wm;
  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<TagComponent> tm;

  GroupManager groupManager;
  TagManager tagManager;

  ArenaClient arenaClient;

  public PlayerControlActionSystem(ArenaClient arenaClient) {
    this.arenaClient = arenaClient;
  }

  @Override
  protected void processSystem() {
    if (Gdx.input.justTouched()) {
      ImmutableBag<Entity> casters = groupManager.getEntities(Group.CASTER);
      if (casters.isEmpty()) {
        Gdx.app.log("DEBUG", "No caster avaiable");
        return;
      }

      Entity cursor = tagManager.getEntity(Tag.CURSOR);
      if (cursor == null) {
        Gdx.app.log("DEBUG", "No cursor avaiable");
        return;
      }

      CursorComponent cursorComponent = cm.get(cursor);

      for (Entity c : casters) {
        if (!isCaster(c)) {
          continue;
        }
        sendPacket(c, cursorComponent);
      }

    }
  }

  private boolean isCaster(Entity c) {
    return sm.has(c);
  }

  private void sendPacket(Entity c, CursorComponent cursorComponent) {
    TagComponent tagComponent = tm.get(c);
    WeaponComponent weaponComponent = wm.get(c);
    if (weaponComponent.getActionId() != -1) {
      CreateCommandEntityPacket packet = new CreateCommandEntityPacket();
      packet.setCommandId(weaponComponent.getActionId());
      packet.setNetworkId(tagComponent.getTag());
      packet.setX(cursorComponent.getSpatial().getX());
      packet.setY(cursorComponent.getSpatial().getY());
      arenaClient.send(Channel.TCP, packet);
    }
  }

}
