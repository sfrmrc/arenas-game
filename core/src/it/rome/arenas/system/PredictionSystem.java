package it.rome.arenas.system;

import it.rome.arena.client.component.LocalNetworkComponent;
import it.rome.arena.client.component.PredictionComponent;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.attribute.Memento;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
@Wire
public class PredictionSystem extends EntityProcessingSystem {

  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<PredictionComponent> pm;
  ComponentMapper<VelocityComponent> vm;
  ComponentMapper<LocalNetworkComponent> nm;

  @SuppressWarnings("unchecked")
  public PredictionSystem() {
    super(Aspect.getAspectForAll(SpatialComponent.class, PredictionComponent.class,
        VelocityComponent.class, LocalNetworkComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    PredictionComponent predictionComponent = pm.get(entity);
    Memento current = predictionComponent.getCurrent();
    if (current == null) {
      return;
    }
    Vector2 speed = vm.get(entity).getSpeed();
    Memento memento =
        new Memento(nm.get(entity).getTick(), current.getX(), current.getY(), current.getAngle(),
            speed);
    predictionComponent.add(memento);
    if (current != null) {
      Spatial spatial = sm.get(entity).getSpatial();
      spatial.setPosition(current.getX(), current.getY());
      spatial.setAngle(current.getAngle());
      /*
       * for (Memento m : predictionComponent.getMemento()) { }
       */
    }
  }

}
