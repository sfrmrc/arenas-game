package it.rome.arenas.system;

import it.rome.game.component.CursorComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

@Wire
public class DebugMousePositionSystem extends EntityProcessingSystem {

  private static final BitmapFont font = new BitmapFont();
  private static final SpriteBatch spriteBatch = new SpriteBatch();

  ComponentMapper<CursorComponent> cm;

  @SuppressWarnings("unchecked")
  public DebugMousePositionSystem() {
    super(Aspect.getAspectForAll(CursorComponent.class));
  }

  @Override
  protected void begin() {
    spriteBatch.begin();
  }

  @Override
  protected void process(Entity entity) {
    font.draw(spriteBatch, "Mouse position: " + cm.get(entity).getSpatial(), 10, 20);
  }

  @Override
  protected void end() {
    spriteBatch.end();
  }

}
