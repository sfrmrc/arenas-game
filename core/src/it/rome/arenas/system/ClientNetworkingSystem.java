package it.rome.arenas.system;

import it.rome.arena.client.ArenaClient;
import it.rome.arena.client.component.PredictionComponent;
import it.rome.arena.shared.listener.packet.AddEntityPacket;
import it.rome.arena.shared.listener.packet.CreatePlayerPacket;
import it.rome.arena.shared.listener.packet.DamageServerMessage;
import it.rome.arena.shared.listener.packet.EntityUpdatePacket;
import it.rome.arena.shared.listener.packet.GameStatePacket;
import it.rome.arena.shared.listener.packet.PlayerDeadPacket;
import it.rome.arena.shared.listener.packet.RemoveEntityPacket;
import it.rome.arena.shared.listener.packet.ServerMessage;
import it.rome.arena.shared.listener.packet.ServerMessagePacket;
import it.rome.arena.shared.listener.packet.ServerUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsWorldPacket;
import it.rome.arena.shared.listener.packet.WarningServerMessage;
import it.rome.arena.shared.listener.packet.WorldInfoPacket;
import it.rome.arenas.ClientGameObjectFactory;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.StateComponent;
import it.rome.game.shared.attribute.Memento;
import it.rome.game.shared.attribute.PlayerState;
import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.impl.PlayerShared;
import it.rome.game.shared.attribute.impl.SpellShared;
import it.rome.game.shared.component.InterpolationComponent;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.HandlerRegistration;
import it.rome.game.shared.listener.packet.ConnectionSucceededPacket;
import it.rome.game.shared.listener.packet.ConnectionTerminatedPacket;
import it.rome.game.utils.CollectionsUtil;

import java.util.List;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
@Wire
public class ClientNetworkingSystem extends VoidEntitySystem {

  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<InterpolationComponent> im;
  ComponentMapper<PredictionComponent> pm;
  ComponentMapper<HealthComponent> hm;
  ComponentMapper<StateComponent<PlayerState>> stm;

  TagManager tagManager;

  DirectorSystem directorSystem;

  ArenaClient arenaClient;
  int tickInterval;

  List<BasePacket<?>> updateQueue = CollectionsUtil.emptyList();
  List<HandlerRegistration> handlers = CollectionsUtil.emptyList();

  public ClientNetworkingSystem(ArenaClient arenaClient, int tickInterval) {
    this.arenaClient = arenaClient;
    this.tickInterval = tickInterval;
    handlers.add(this.arenaClient.addHandler(ConnectionSucceededPacket.TYPE,
        new ConnectionSucceededListener()));
    handlers.add(this.arenaClient.addHandler(ConnectionTerminatedPacket.TYPE,
        new ConnectionTerminatedListener()));
    handlers.add(this.arenaClient.addHandler(ServerUpdatePacket.TYPE, new ServerUpdateListener()));
    handlers.add(this.arenaClient.addHandler(AddEntityPacket.TYPE, new AddEntityListener()));
    handlers.add(this.arenaClient.addHandler(CreatePlayerPacket.TYPE, new CreatePlayerListener()));
    handlers.add(this.arenaClient.addHandler(RemoveEntityPacket.TYPE, new RemoveEntityListener()));
    handlers.add(this.arenaClient.addHandler(WorldInfoPacket.TYPE, new WorldInfoListener()));
    handlers
        .add(this.arenaClient.addHandler(ServerMessagePacket.TYPE, new ServerMessageListener()));
    handlers
        .add(this.arenaClient.addHandler(StatsWorldPacket.TYPE, new ServerStatsWorldListener()));
    handlers.add(this.arenaClient.addHandler(GameStatePacket.TYPE, new GameStateListener()));
  }

  @Override
  protected void processSystem() {
    synchronized (updateQueue) {
      for (BasePacket<?> packet : updateQueue) {
        if (packet.getType() == ServerUpdatePacket.TYPE) {
          handleUpdate(packet);
        } else if (packet.getType() == AddEntityPacket.TYPE) {
          handleAddEntity(packet);
        } else if (packet.getType() == CreatePlayerPacket.TYPE) {
          handleCreatePlayer(packet);
        } else if (packet.getType() == RemoveEntityPacket.TYPE) {
          handleRemoveEntity(packet);
        } else if (packet.getType() == DamageServerMessage.TYPE) {
          handleDamage(packet);
        } else if (packet.getType() == StatsUpdatePacket.TYPE) {
          handleStatsUpdate(packet);
        } else if (packet.getType() == PlayerDeadPacket.TYPE) {
          handleDead(packet);
        } else if (packet.getType() == GameStatePacket.TYPE) {
          handleGameState(packet);
        } else if (packet.getType() == WarningServerMessage.TYPE) {
          handleWarning(packet);
        }
      }
      updateQueue.clear();
    }
  }

  @Override
  protected void dispose() {
    for (HandlerRegistration h : handlers) {
      h.removeHandler();
    }
  }

  private void handleGameState(BasePacket<?> serverCommand) {
    GameStatePacket packet = (GameStatePacket) serverCommand;
    directorSystem.setGameState(packet.getGameState());
    ClientGameObjectFactory.createFadingText(packet.getGameState().toString());
  }

  private void handleDead(BasePacket<?> serverCommand) {
    PlayerDeadPacket playerDeadPacket = (PlayerDeadPacket) serverCommand;
    Entity entity = tagManager.getEntity(playerDeadPacket.getNetworkId());
    if (entity != null) {
      ClientGameObjectFactory.createFadingText(playerDeadPacket.getNetworkId() + " is dead");
    }
  }

  private void handleWarning(BasePacket<?> serverCommand) {
    WarningServerMessage packet = (WarningServerMessage) serverCommand;
    ClientGameObjectFactory.createFadingText(packet.getText());
  }

  private void handleStatsUpdate(BasePacket<?> serverCommand) {
    StatsUpdatePacket statsUpdatePacket = (StatsUpdatePacket) serverCommand;
    Entity entity = tagManager.getEntity(statsUpdatePacket.getNetworkId());
    if (entity != null) {
      StatsComponent statsComponent = entity.getComponent(StatsComponent.class);
      int onServer = statsUpdatePacket.getStats().getHealth().getLife();
      int onClient = statsComponent.getStats().getHealth().getLife();
      int difference = onServer - onClient;
      statsComponent.getStats().getHealth().increase(difference);
    }
  }

  private void handleDamage(BasePacket<?> serverCommand) {
    DamageServerMessage damageServerMessage = (DamageServerMessage) serverCommand;
    Entity target = tagManager.getEntity(damageServerMessage.getNetworkId());
    if (target != null) {
      String damage = damageServerMessage.getDamage().toString();
      SpatialComponent spatialComponent = sm.get(target);
      Spatial spatial = spatialComponent.getSpatial();
      ClientGameObjectFactory.createFadingText(damage, spatial.getX(), spatial.getY());
    }
  }

  private void handleRemoveEntity(BasePacket<?> serverCommand) {
    RemoveEntityPacket packet = (RemoveEntityPacket) serverCommand;
    Entity entity = tagManager.getEntity(packet.getNetworkId());
    if (entity == null) {
      Gdx.app.log("ERROR", "Client: entity already removed");
      return;
    }
    entity.deleteFromWorld();
  }

  private void handleCreatePlayer(BasePacket<?> serverCommand) {
    CreatePlayerPacket createPlayerPacket = (CreatePlayerPacket) serverCommand;
    ClientGameObjectFactory.createPlayer(createPlayerPacket.getShared(),
        createPlayerPacket.getSpellBook());
  }

  private void handleAddEntity(BasePacket<?> serverCommand) {
    AddEntityPacket addEntityPacket = (AddEntityPacket) serverCommand;
    String tag = addEntityPacket.getNetworkId();
    if (!tagManager.isRegistered(tag)) {
      Shared shared = addEntityPacket.getShared();
      if (shared instanceof PlayerShared) {
        ClientGameObjectFactory.createRemotePlayer((PlayerShared) shared);
      } else if (shared instanceof SpellShared) {
        ClientGameObjectFactory.createObject((SpellShared) shared);
      }
    }
  }

  private void handleUpdate(BasePacket<?> serverCommand) {
    ServerUpdatePacket serverUpdatePacket = (ServerUpdatePacket) serverCommand;
    for (EntityUpdatePacket entityUpdate : serverUpdatePacket.getEntityUpdates()) {
      if (entityUpdate == null) {
        continue;
      }
      Entity entity = tagManager.getEntity(entityUpdate.getNetworkId());
      if (entity == null) {
        Gdx.app.log("ERROR", "Client: error updating entity " + entityUpdate.getNetworkId());
        continue;
      }

      Memento current =
          new Memento(serverUpdatePacket.getPlayerInputTick(), entityUpdate.getX(),
              entityUpdate.getY(), entityUpdate.getAngle(), entityUpdate.getSpeed());

      if (im.has(entity)) {
        InterpolationComponent interpolationComponent = im.get(entity);
        interpolationComponent.swap(current);
        interpolate(entity);
      }
      if (pm.has(entity)) {
        PredictionComponent predictionComponent = pm.get(entity);
        predictionComponent.setCurrent(current);
      }

    }
  }

  private void interpolate(Entity entity) {
    Memento old = im.get(entity).getOld();
    Memento current = im.get(entity).getCurrent();
    Spatial spatial = sm.get(entity).getSpatial();
    Vector2 next = new Vector2(current.getX(), current.getY());
    Vector2 position =
        new Vector2(old.getX(), old.getY()).lerp(next, getAlphaInterpolation(im.get(entity)));
    spatial.setPosition(position.x, position.y);
    spatial.setAngle(current.getAngle());
    // FIXME: it works only on players ... I can do better then this :(
    if (stm.has(entity)) {
      StateComponent<PlayerState> stateComponent = stm.get(entity);
      float deltaX = old.getX() - current.getX();
      float deltaY = old.getY() - current.getY();
      if (deltaX < 0) {
        stateComponent.change(PlayerState.W_RIGHT);
      }
      if (deltaX > 0) {
        stateComponent.change(PlayerState.W_LEFT);
      }
      if (deltaY < 0) {
        stateComponent.change(PlayerState.W_UP);
      }
      if (deltaY > 0) {
        stateComponent.change(PlayerState.W_DOWN);
      }
      if (deltaX == 0 && deltaY == 0) {
        stateComponent.change(PlayerState.NOPE);
      }
    }
  }

  private float getAlphaInterpolation(InterpolationComponent interpolationComponent) {
    float alpha =
        (System.currentTimeMillis() - interpolationComponent.getLastUpdate()) / tickInterval;
    return alpha;
  }

  private void addUpdateQueue(BasePacket<?> packet) {
    synchronized (updateQueue) {
      updateQueue.add(packet);
    }
  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class ConnectionSucceededListener implements ConnectionSucceededPacket.Handler {
    @Override
    public void onConnectionSucceeded(ConnectionSucceededPacket packet, Connection connection) {
      Gdx.app.log("INFO", "Client: connection succeeded");
    }
  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class ConnectionTerminatedListener implements ConnectionTerminatedPacket.Handler {
    @Override
    public void onConnectionTerminated(ConnectionTerminatedPacket packet, Connection connection) {
      Entity player = tagManager.getEntity("user1");
      if (player != null) {
        player.deleteFromWorld();
      }
      Gdx.app.log("INFO", "Client: connection terminated");
    }
  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class ServerUpdateListener implements ServerUpdatePacket.Handler {

    @Override
    public void onServerUpdated(ServerUpdatePacket packet, Connection connection) {
      addUpdateQueue(packet);
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class AddEntityListener implements AddEntityPacket.Handler {

    @Override
    public void onEntityAdded(AddEntityPacket packet, Connection connection) {
      addUpdateQueue(packet);
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class CreatePlayerListener implements CreatePlayerPacket.Handler {

    @Override
    public void onCreatePlayer(CreatePlayerPacket packet, Connection connection) {
      addUpdateQueue(packet);
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class RemoveEntityListener implements RemoveEntityPacket.Handler {

    @Override
    public void onRemoveEntity(RemoveEntityPacket packet, Connection connection) {
      addUpdateQueue(packet);
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class WorldInfoListener implements WorldInfoPacket.Handler {

    @Override
    public void onWorldInfoReceived(WorldInfoPacket packet, Connection connection) {
      for (AddEntityPacket p : packet.getAddEntities()) {
        addUpdateQueue(p);
      }
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class ServerMessageListener implements ServerMessagePacket.Handler {

    @Override
    public void onMessageReceived(ServerMessagePacket packet) {
      for (ServerMessage<?> m : packet.getMessages()) {
        addUpdateQueue(m);
      }
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  private class ServerStatsWorldListener implements StatsWorldPacket.Handler {

    @Override
    public void onStatsWorldReceived(StatsWorldPacket packet) {
      for (StatsUpdatePacket p : packet.getStatsUpdatePackets()) {
        addUpdateQueue(p);
      }
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class GameStateListener implements GameStatePacket.Handler {

    @Override
    public void onGameStateChanged(GameStatePacket packet) {
      addUpdateQueue(packet);
    }

  }

}
