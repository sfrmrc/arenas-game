package it.rome.arenas.system;

import it.rome.arena.client.component.LocalNetworkComponent;
import it.rome.arenas.stage.CombatStage;
import it.rome.arenas.ui.ActionBarScript.SpellClickListener;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.shared.component.WeaponComponent;

import java.util.Collection;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
@Wire
public class HUDSystem extends EntityProcessingSystem {

  ComponentMapper<TagComponent> tm;
  ComponentMapper<StatsComponent> sm;
  ComponentMapper<WeaponComponent> wm;

  TagManager tagManager;
  GroupManager groupManager;

  CombatStage stage;

  @SuppressWarnings("unchecked")
  public HUDSystem() {
    super(Aspect.getAspectForAll(LocalNetworkComponent.class, TagComponent.class,
        StatsComponent.class, HealthComponent.class, WeaponComponent.class));
  }

  @Override
  protected void initialize() {
    stage = new CombatStage();
  }

  @Override
  protected void inserted(Entity entity) {
    super.inserted(entity);
    TagComponent tagComponent = tm.get(entity);
    WeaponComponent weaponComponent = wm.get(entity);
    Collection<Spell> spellBook = weaponComponent.getSpellBook();
    Stats stats = sm.get(entity).getStats();
    String tag = tagComponent.getTag();
    stage.addPlayer(tag, stats, spellBook, new PlayerSpellSelected(tag));
  }

  @Override
  protected void process(Entity entity) {
    stage.act();
    stage.draw();
  }

  @Override
  protected void dispose() {
    stage.dispose();
    super.dispose();
  }

  private void setSpell(Entity entity, Spell spell) {
    if (wm.has(entity)) {
      WeaponComponent weaponComponent = wm.get(entity);
      weaponComponent.setActionId(spell.getId());
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class PlayerSpellSelected implements SpellClickListener {

    final String tag;

    public PlayerSpellSelected(String tag) {
      this.tag = tag;
    }

    @Override
    public void onSpellClicked(Spell spell) {
      Entity entity = tagManager.getEntity(tag);
      if (entity != null) {
        setSpell(entity, spell);
      }
    }

  }

}
