package it.rome.arenas.system;

import it.rome.arena.client.ArenaClient;
import it.rome.arena.client.component.LocalNetworkComponent;
import it.rome.arena.shared.listener.packet.ClientUpdatePacket;
import it.rome.arena.shared.listener.packet.Command;
import it.rome.game.component.InputComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.StateComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.Channel;
import it.rome.game.shared.attribute.PlayerState;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
@Wire
public class PlayerControlMovementSystem extends EntityProcessingSystem {

  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<StateComponent<PlayerState>> stm;
  ComponentMapper<VelocityComponent> vm;
  ComponentMapper<LocalNetworkComponent> nm;

  ArenaClient arenaClient;

  @SuppressWarnings("unchecked")
  public PlayerControlMovementSystem(ArenaClient arenaClient) {
    super(Aspect.getAspectForAll(InputComponent.class, VelocityComponent.class,
        LocalNetworkComponent.class, StateComponent.class));
    this.arenaClient = arenaClient;
  }

  @Override
  protected void process(Entity entity) {
    LocalNetworkComponent localNetworkComponent = nm.get(entity);
    VelocityComponent velocityComponent = vm.get(entity);
    Vector2 velocity = velocityComponent.getSpeed();
    velocity.setZero();
    StateComponent<PlayerState> stateComponent = stm.get(entity);
    PlayerState state = PlayerState.NOPE;
    Command command = Command.NONE;
    if (Gdx.input.isKeyPressed(Keys.A)) {
      command = Command.LEFT;
      state = PlayerState.W_LEFT;
    }
    if (Gdx.input.isKeyPressed(Keys.D)) {
      command = Command.RIGHT;
      state = PlayerState.W_RIGHT;
    }
    if (Gdx.input.isKeyPressed(Keys.W)) {
      command = Command.UP;
      state = PlayerState.W_UP;
    }
    if (Gdx.input.isKeyPressed(Keys.S)) {
      command = Command.DOWN;
      state = PlayerState.W_DOWN;
    }
    stateComponent.change(state);
    localNetworkComponent.incrementTick();
    ClientUpdatePacket clientUpdatePacket =
        new ClientUpdatePacket(localNetworkComponent.getNetworkId(), command);
    clientUpdatePacket.setInputTick(localNetworkComponent.getTick());
    command.apply(velocity);
    arenaClient.send(Channel.UDP, clientUpdatePacket);
  }
}
