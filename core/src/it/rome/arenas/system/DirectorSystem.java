package it.rome.arenas.system;

import it.rome.arena.shared.GameState;

import com.artemis.annotations.Wire;
import com.artemis.systems.VoidEntitySystem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class DirectorSystem extends VoidEntitySystem {

  private GameState gameState;

  @Override
  protected void processSystem() {}

  public void setGameState(GameState gameState) {
    this.gameState = gameState;
  }

  public GameState getGameState() {
    return gameState;
  }

}
