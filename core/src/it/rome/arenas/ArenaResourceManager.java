package it.rome.arenas;

import com.badlogic.gdx.Gdx;
import com.uwsoft.editor.renderer.resources.ResourceManager;
import com.uwsoft.editor.renderer.utils.MySkin;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ArenaResourceManager extends ResourceManager {

  private final MySkin skin = new MySkin(Gdx.files.internal("uiskin.json"));

  @Override
  public MySkin getSkin() {
    return skin;
  }

}
