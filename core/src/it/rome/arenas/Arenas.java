package it.rome.arenas;

import it.rome.arenas.screen.LoginScreen;

import com.badlogic.gdx.Game;

public class Arenas extends Game {

  @Override
  public void create() {
    setScreen(new LoginScreen(this));
  }
	
}
