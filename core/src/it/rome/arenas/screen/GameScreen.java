package it.rome.arenas.screen;

import it.rome.arena.shared.GameState;
import it.rome.arena.shared.Layers;
import it.rome.arena.shared.listener.packet.ClientReadyPacket;
import it.rome.arenas.ClientGameObjectFactory;
import it.rome.arenas.system.ClientNetworkingSystem;
import it.rome.arenas.system.DirectorSystem;
import it.rome.arenas.system.HUDSystem;
import it.rome.arenas.system.PlayerControlActionSystem;
import it.rome.arenas.system.PlayerControlMovementSystem;
import it.rome.arenas.system.PredictionSystem;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.shared.Channel;
import it.rome.game.shared.template.ArenaTemplateGroups;
import it.rome.game.system.CameraBoundsSystem;
import it.rome.game.system.CameraFocusSystem;
import it.rome.game.system.HealtIndicatorSystem;
import it.rome.game.system.MapRenderSystem;
import it.rome.game.system.MouseCursorSystem;
import it.rome.game.system.PhysicsMovementSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.RenderableSystem;
import it.rome.game.system.SpriteAnimationSystem;
import it.rome.game.system.SpritePositionSystem;
import it.rome.game.system.render.Layer;
import it.rome.game.system.render.RenderableLayer;

import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.UuidEntityManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen extends ArenaBaseScreen {

  float width, height;
  float halfWidth, halfHeight;

  String username;
  String arenaName;
  RenderableLayer renderableLayer;
  OrthographicCamera hudCamera;
  BodyBuilder bodyBuilder;
  DirectorSystem directorSystem;

  public GameScreen(Game game, String username, String arenaName) {
    super(game);
    this.username = username;
    this.arenaName = arenaName;
    this.bodyBuilder = new BodyBuilder(physicWorld);
    initArtemisWorld();
    ClientGameObjectFactory.createAim();
    ClientReadyPacket packet = new ClientReadyPacket(username);
    arenaClient.send(Channel.TCP, packet);
  }

  @Override
  protected void initWorld() {
    this.world.setManager(new UuidEntityManager());
    this.world.setManager(new TagManager());
    this.world.setManager(new GroupManager());
    directorSystem = new DirectorSystem();
    this.world.setSystem(directorSystem);
  }

  private void initArtemisWorld() {
    ArenaTemplateGroups arenaTemplateGroups = new ArenaTemplateGroups(physicWorld);
    this.world.setSystem(new MouseCursorSystem(camera));
    this.world.setSystem(new PlayerControlMovementSystem(arenaClient));
    this.world.setSystem(new CameraFocusSystem(camera));
    this.world.setSystem(new CameraBoundsSystem(camera, 1248, 1248));
    this.world.setSystem(new PlayerControlActionSystem(arenaClient));
    this.world.setSystem(new ClientNetworkingSystem(arenaClient, 50));
    this.world.setSystem(new PredictionSystem());
    this.world.setSystem(new PhysicsMovementSystem());
    this.world.setSystem(new PhysicsSystem(physicWorld));
    this.world.setSystem(new SpritePositionSystem());
    this.world.setSystem(new SpriteAnimationSystem());
    this.world.setSystem(new MapRenderSystem(camera, arenaName, arenaTemplateGroups));
    this.world.setSystem(new HealtIndicatorSystem());
    this.world.setSystem(new RenderableSystem(camera, renderableLayer));
    this.world.setSystem(new HUDSystem());
    ClientGameObjectFactory.init(world, physicWorld);
  }

  protected void init() {
    SpriteBatch spriteBatch = new SpriteBatch();
    Layer messages = new Layer(Layers.MESSAGE, camera, spriteBatch);
    Layer player = new Layer(Layers.PLAYER, camera, spriteBatch);
    Layer others = new Layer(Layers.OTHERS, camera, spriteBatch);
    Layer spell = new Layer(Layers.SPELL, camera, spriteBatch);
    renderableLayer = new RenderableLayer();
    renderableLayer.add("messages", messages);
    renderableLayer.add("players", player);
    renderableLayer.add("spell", spell);
    renderableLayer.add("others", others);
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    this.world.setDelta(delta);
    this.world.process();

    if (directorSystem.getGameState() == GameState.ENDED) {
      this.world.dispose();
      game.setScreen(new HomeScreen(game, username));
    }

  }

  @Override
  public void dispose() {
    this.world.dispose();
    arenaClient.disconnect();
    super.dispose();
  }

}
