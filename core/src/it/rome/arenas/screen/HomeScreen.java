package it.rome.arenas.screen;

import it.rome.arena.shared.listener.packet.BeginArenaQueue;
import it.rome.arena.shared.listener.packet.EndArenaQueuePacket;
import it.rome.arena.shared.listener.packet.RequestArenaQueuePacket;
import it.rome.arena.shared.listener.packet.RequestUserInfoPacket;
import it.rome.arena.shared.listener.packet.UserInfoPacket;
import it.rome.arenas.stage.HomeStage;
import it.rome.game.shared.Channel;
import it.rome.game.shared.listener.HandlerRegistration;
import it.rome.game.utils.CollectionsUtil;

import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class HomeScreen extends ArenaBaseScreen {

  private final String username;
  private final HomeStage stage;

  private List<HandlerRegistration> handlers = CollectionsUtil.emptyList();

  public HomeScreen(Game game, String username) {
    super(game);
    this.username = username;
    this.stage = new HomeStage(this);
    HandlerRegistration handlerRegistration =
        arenaClient.addHandler(EndArenaQueuePacket.TYPE, new EndArenaQueueListener());
    handlers.add(handlerRegistration);
  }

  private void initUserInfo() {
    RequestUserInfoPacket packet = new RequestUserInfoPacket();
    HandlerRegistration handlerRegistration =
        arenaClient.addHandler(UserInfoPacket.TYPE, new UserInfoListener());
    handlers.add(handlerRegistration);
    stage.setUser(username);
    arenaClient.send(Channel.TCP, packet);
  }

  public void arenaQueue() {
    RequestArenaQueuePacket packet = new RequestArenaQueuePacket();
    HandlerRegistration handlerRegistration =
        arenaClient.addHandler(BeginArenaQueue.TYPE, new BeginArenaListener());
    handlers.add(handlerRegistration);
    arenaClient.send(Channel.TCP, packet);
  }

  @Override
  public void show() {
    super.show();
    initUserInfo();
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void dispose() {
    stage.dispose();
    clearHandlers();
    super.dispose();
  }

  private void clearHandlers() {
    for (HandlerRegistration h : handlers) {
      h.removeHandler();
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class EndArenaQueueListener implements EndArenaQueuePacket.Handler {
    @Override
    public void onQueueEnded(final EndArenaQueuePacket endQueuePacket) {
      Gdx.app.postRunnable(new Runnable() {
        @Override
        public void run() {
          game.setScreen(new GameScreen(game, username, endQueuePacket.getArenaName()));
          clearHandlers();
        }
      });
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class UserInfoListener implements UserInfoPacket.Handler {
    @Override
    public void onUserInfoReceived(final UserInfoPacket packet) {
      Gdx.app.postRunnable(new Runnable() {
        @Override
        public void run() {
          stage.show(packet.getStats());
        }
      });
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private class BeginArenaListener implements BeginArenaQueue.Handler {
    @Override
    public void onArenaQueued(BeginArenaQueue packet) {
      stage.showQueue();
    }
  }

}
