package it.rome.arenas.screen;

import it.rome.arena.shared.listener.packet.LoginPacket;
import it.rome.arena.shared.listener.packet.LoginSucceededPacket;
import it.rome.arena.shared.listener.packet.OperationFailedPacket;
import it.rome.arenas.stage.LoginStage;
import it.rome.game.shared.Channel;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.HandlerRegistration;
import it.rome.game.utils.CollectionsUtil;

import java.net.ConnectException;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class LoginScreen extends ArenaBaseScreen {

  private static final int LOGIN_TIMEOUT = 10000;

  private LoginStage stage;
  private BasePacket<?> response = null;
  private final List<HandlerRegistration> handlers = CollectionsUtil.emptyList();

  public LoginScreen(final Game game) {
    super(game);
    initListeners();
    stage = new LoginStage(this);
  }

  private void initListeners() {
    handlers.add(arenaClient.addHandler(LoginSucceededPacket.TYPE, new LoginSucceededListener()));
    handlers.add(arenaClient.addHandler(OperationFailedPacket.TYPE, new OperationFailedListener()));
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    stage.act(delta);
    stage.draw();
  }

  @Override
  public void dispose() {
    stage.dispose();
    clearListener();
    super.dispose();
  }

  public void login(String username, String password) {
    try {
      arenaClient.connect();
      received(null);
      arenaClient.send(Channel.TCP, new LoginPacket(username, password));
      waiting();
    } catch (ConnectException e) {
      Gdx.app.log("ERROR", "Client: error " + e.getMessage());
      throwError(e.getMessage());
    } catch (InterruptedException e) {
      Gdx.app.log("ERROR", "Client: error " + e.getMessage());
      throwError(e.getMessage());
    }
  }

  private void waiting() throws InterruptedException {
    long start = System.currentTimeMillis();
    long waiting = 0;
    while (response == null && waiting < LOGIN_TIMEOUT) {
      Thread.sleep(100);
      waiting = System.currentTimeMillis() - start;
    }
    if (response == null) {
      throwError("Unknown error");
      return;
    }
    if (response == null || response.getType() == OperationFailedPacket.TYPE) {
      OperationFailedPacket packet = (OperationFailedPacket) response;
      Gdx.app.log("ERROR", "Client: error " + packet.getOperation());
      throwError(packet.getOperation());
      return;
    }
    if (response.getType() == LoginSucceededPacket.TYPE) {
      LoginSucceededPacket packet = (LoginSucceededPacket) response;
      game.setScreen(new HomeScreen(game, packet.getNetworkId()));
      return;
    }
  }

  private void throwError(String message) {
    stage.showErrorMessage(message);
  }

  private void received(BasePacket<?> response) {
    this.response = response;
  }

  private void clearListener() {
    for (HandlerRegistration h : handlers) {
      h.removeHandler();
    }
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private final class LoginSucceededListener implements LoginSucceededPacket.Handler {

    @Override
    public void onLoggedIn(LoginSucceededPacket packet, Connection connection) {
      received(packet);
    }

  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  private final class OperationFailedListener implements OperationFailedPacket.Handler {

    @Override
    public void onFailed(OperationFailedPacket packet, Connection connection) {
      received(packet);
    }

  }

}
