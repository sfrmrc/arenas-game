package it.rome.arenas.screen;

import it.rome.arena.client.ArenaClient;
import it.rome.game.screen.AbstractGameBaseScreen;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public abstract class ArenaBaseScreen extends AbstractGameBaseScreen {

  private static final String CLIENT_PROPERTIES = "client.properties";
  private static final Properties properties = new Properties();

  protected static final ArenaClient arenaClient;

  static {
    String server = "localhost";
    if (Gdx.files.local(CLIENT_PROPERTIES).exists()) {
      try {
        InputStream is = Gdx.files.local(CLIENT_PROPERTIES).read();
        properties.load(is);
      } catch (IOException e) {
        e.printStackTrace();
      }
      server = properties.getProperty("server", "localhost");
    }
    arenaClient = new ArenaClient(server);
  }

  public ArenaBaseScreen(Game game) {
    super(game);
  }

  @Override
  protected void init() {}

  @Override
  protected void initWorld() {}

  @Override
  protected void initPhysicWorld() {}

}
