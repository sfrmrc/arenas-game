package it.rome.arena.shared;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class Tag {

  public static final String CURSOR = "cursor";
  public static final String PLAYER = "player";

}
