package it.rome.arena.shared.config;

import it.rome.arena.shared.Class;
import it.rome.arena.shared.GameState;
import it.rome.arena.shared.Race;
import it.rome.arena.shared.SpellCast;
import it.rome.arena.shared.SpellType;
import it.rome.arena.shared.listener.packet.AddEntityPacket;
import it.rome.arena.shared.listener.packet.BeginArenaQueue;
import it.rome.arena.shared.listener.packet.ClientReadyPacket;
import it.rome.arena.shared.listener.packet.ClientUpdatePacket;
import it.rome.arena.shared.listener.packet.Command;
import it.rome.arena.shared.listener.packet.CreateCommandEntityPacket;
import it.rome.arena.shared.listener.packet.CreatePlayerPacket;
import it.rome.arena.shared.listener.packet.DamageServerMessage;
import it.rome.arena.shared.listener.packet.EndArenaQueuePacket;
import it.rome.arena.shared.listener.packet.EntityUpdatePacket;
import it.rome.arena.shared.listener.packet.GameStatePacket;
import it.rome.arena.shared.listener.packet.LoginPacket;
import it.rome.arena.shared.listener.packet.LoginSucceededPacket;
import it.rome.arena.shared.listener.packet.OperationFailedPacket;
import it.rome.arena.shared.listener.packet.OperationSucceededPacket;
import it.rome.arena.shared.listener.packet.PlayerDeadPacket;
import it.rome.arena.shared.listener.packet.RemoveEntityPacket;
import it.rome.arena.shared.listener.packet.RequestArenaQueuePacket;
import it.rome.arena.shared.listener.packet.RequestUserInfoPacket;
import it.rome.arena.shared.listener.packet.ServerMessage;
import it.rome.arena.shared.listener.packet.ServerMessagePacket;
import it.rome.arena.shared.listener.packet.ServerUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsWorldPacket;
import it.rome.arena.shared.listener.packet.UserInfoPacket;
import it.rome.arena.shared.listener.packet.WarningServerMessage;
import it.rome.arena.shared.listener.packet.WorldInfoPacket;
import it.rome.game.attribute.impl.HealthImpl;
import it.rome.game.attribute.impl.PointsImpl;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.UserId;
import it.rome.game.shared.attribute.Attributes;
import it.rome.game.shared.attribute.Attributes.Attribute;
import it.rome.game.shared.attribute.Attributes.TYPE;
import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.attribute.impl.BaseShared;
import it.rome.game.shared.attribute.impl.MotionShared;
import it.rome.game.shared.attribute.impl.PlayerShared;
import it.rome.game.shared.attribute.impl.SpellShared;
import it.rome.game.shared.listener.packet.Packet;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class ArenaConfig {

  private ArenaConfig() {}

  public static final int DEFAULT_TCP_PORT = 31055;
  public static final int DEFAULT_UDP_PORT = 32055;
  public static final int LATENCY = 5000;

  public static final Packet packets = new Packet(UserId.class, ClientUpdatePacket.class,
      ClientInfo.class, LoginPacket.class, OperationFailedPacket.class, Command.class,
      OperationSucceededPacket.class, LoginSucceededPacket.class, EntityUpdatePacket[].class,
      EntityUpdatePacket.class, ServerUpdatePacket.class, Vector2.class, AddEntityPacket.class,
      AddEntityPacket[].class, CreatePlayerPacket.class, RemoveEntityPacket.class,
      CreateCommandEntityPacket.class, WorldInfoPacket.class, Stats.class, Stats[].class,
      HashMap.class, TYPE.class, Attributes.class, Attribute.class, PlayerShared.class,
      SpellShared.class, Shared.class, SpatialImpl.class, Race.class, MotionShared.class,
      ServerMessagePacket.class, ServerMessage[].class, ServerMessage.class,
      DamageServerMessage.class, WarningServerMessage.class, StatsUpdatePacket.class, StatsUpdatePacket[].class,
      StatsWorldPacket.class, ClientReadyPacket.class, PlayerDeadPacket.class,
      GameStatePacket.class, GameState.class, BaseShared.class, RequestUserInfoPacket.class,
      UserInfoPacket.class, RequestArenaQueuePacket.class, BeginArenaQueue.class,
      EndArenaQueuePacket.class, HealthImpl.class, PointsImpl.class, Class.class, ArrayList.class,
      Spell.class, SpellType.class, SpellCast.class);

}
