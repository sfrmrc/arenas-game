package it.rome.arena.shared;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class Layers {

  public static int PLAYER = 1;
  public static int MESSAGE = 2;
  public static int WALL = 3;
  public static int SPELL = 4;
  public static int OTHERS = 5;

}
