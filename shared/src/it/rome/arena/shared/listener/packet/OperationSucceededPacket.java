package it.rome.arena.shared.listener.packet;

import com.esotericsoftware.kryonet.Connection;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class OperationSucceededPacket extends BasePacket<OperationSucceededPacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public interface Handler extends EventHandler {
    public void onOperationSucceeded(OperationSucceededPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  protected String operation;

  public OperationSucceededPacket(String operation) {
    super();
    this.operation = operation;
  }

  public String getOperation() {
    return operation;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onOperationSucceeded(this, connection);
  }

}
