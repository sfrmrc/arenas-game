package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class CreateCommandEntityPacket extends BasePacket<CreateCommandEntityPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onCommandEntityReceived(CreateCommandEntityPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private String networkId;
  private float x, y;
  private int commandId;

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public void setCommandId(int commandId) {
    this.commandId = commandId;
  }

  public int getCommandId() {
    return commandId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onCommandEntityReceived(this, connection);
  }

}
