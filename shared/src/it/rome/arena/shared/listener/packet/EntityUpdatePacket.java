package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class EntityUpdatePacket extends BasePacket<EntityUpdatePacket.Handler> {

  public interface Handler extends EventHandler {}

  public static final Type<Handler> TYPE = new Type<Handler>();

  String networkId;
  float x, y, angle;
  Vector2 speed;
  
  public EntityUpdatePacket(String networkId, float x, float y, float angle, Vector2 speed) {
    super();
    this.networkId = networkId;
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.speed = speed;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  public String getNetworkId() {
    return networkId;
  }

  public float getX() {
    return x;
  }

  public float getY() {
    return y;
  }

  public float getAngle() {
    return angle;
  }

  public Vector2 getSpeed() {
    return speed;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    throw new UnsupportedOperationException("EntityUpdatePacket is not supported");
  }

}
