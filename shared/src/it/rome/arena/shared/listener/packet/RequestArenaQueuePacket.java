package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RequestArenaQueuePacket extends BasePacket<RequestArenaQueuePacket.Handler> {

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public interface Handler extends EventHandler {
    public void onArenaQueueRequested(RequestArenaQueuePacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onArenaQueueRequested(this, connection);
  }

}
