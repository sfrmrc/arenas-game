package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class UserInfoPacket extends BasePacket<UserInfoPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public interface Handler extends EventHandler {
    public void onUserInfoReceived(UserInfoPacket packet);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  final Stats[] stats;

  public UserInfoPacket(Stats[] stats) {
    super();
    this.stats = stats;
  }

  public Stats[] getStats() {
    return stats;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onUserInfoReceived(this);
  }

}
