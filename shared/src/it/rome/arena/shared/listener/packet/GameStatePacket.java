package it.rome.arena.shared.listener.packet;

import it.rome.arena.shared.GameState;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class GameStatePacket extends ServerMessage<GameStatePacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onGameStateChanged(GameStatePacket packet);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private GameState gameState;

  public GameStatePacket(GameState gameState) {
    super();
    this.gameState = gameState;
  }

  public GameState getGameState() {
    return gameState;
  }

  public void setGameState(GameState gameState) {
    this.gameState = gameState;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onGameStateChanged(this);
  }

}
