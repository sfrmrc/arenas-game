package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class LoginPacket extends BasePacket<LoginPacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public interface Handler extends EventHandler {
    public void onLogin(LoginPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String userName;
  private final String password;

  public LoginPacket(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public String getUserName() {
    return userName;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onLogin(this, connection);
  }

}
