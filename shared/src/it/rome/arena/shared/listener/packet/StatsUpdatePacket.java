package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class StatsUpdatePacket extends BasePacket<StatsUpdatePacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {}

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String networkId;
  private final Stats stats;

  public StatsUpdatePacket(String networkId, Stats stats) {
    super();
    this.networkId = networkId;
    this.stats = stats;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Stats getStats() {
    return stats;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    throw new UnsupportedOperationException("StatsUpdatePacket not supported yet");
  }
}
