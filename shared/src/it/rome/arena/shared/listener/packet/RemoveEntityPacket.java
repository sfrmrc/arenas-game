package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class RemoveEntityPacket extends BasePacket<RemoveEntityPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onRemoveEntity(RemoveEntityPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private String networkId;

  public RemoveEntityPacket(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onRemoveEntity(this, connection);
  }

}
