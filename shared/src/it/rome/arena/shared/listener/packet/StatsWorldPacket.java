package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class StatsWorldPacket extends BasePacket<StatsWorldPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onStatsWorldReceived(StatsWorldPacket packet);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  StatsUpdatePacket[] statsUpdatePackets;

  public StatsUpdatePacket[] getStatsUpdatePackets() {
    return statsUpdatePackets;
  }

  public void setStatsUpdatePackets(StatsUpdatePacket[] statsUpdatePackets) {
    this.statsUpdatePackets = statsUpdatePackets;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onStatsWorldReceived(this);
  }

}
