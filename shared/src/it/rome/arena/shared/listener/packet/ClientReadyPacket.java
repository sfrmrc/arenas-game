package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ClientReadyPacket extends BasePacket<ClientReadyPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onClientReady(ClientReadyPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String networkId;

  public ClientReadyPacket(String networkId) {
    super();
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onClientReady(this, connection);
  }

}
