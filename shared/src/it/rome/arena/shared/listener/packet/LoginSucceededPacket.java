package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class LoginSucceededPacket extends BasePacket<LoginSucceededPacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public interface Handler extends EventHandler {
    public void onLoggedIn(LoginSucceededPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private String networkId;

  public LoginSucceededPacket(String networkId) {
    super();
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onLoggedIn(this, connection);
  }

}
