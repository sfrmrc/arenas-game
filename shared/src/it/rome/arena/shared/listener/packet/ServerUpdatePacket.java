package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class ServerUpdatePacket extends BasePacket<ServerUpdatePacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onServerUpdated(ServerUpdatePacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  int tickNum;
  int playerInputTick;
  EntityUpdatePacket[] entityUpdates;

  public Type<Handler> getType() {
    return TYPE;
  }

  public int getTickNum() {
    return tickNum;
  }

  public void setTickNum(int tickNum) {
    this.tickNum = tickNum;
  }

  public int getPlayerInputTick() {
    return playerInputTick;
  }

  public void setPlayerInputTick(int playerInputTick) {
    this.playerInputTick = playerInputTick;
  }

  public EntityUpdatePacket[] getEntityUpdates() {
    return entityUpdates;
  }

  public void setEntityUpdates(EntityUpdatePacket[] entityUpdates) {
    this.entityUpdates = entityUpdates;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onServerUpdated(this, connection);
  }

}
