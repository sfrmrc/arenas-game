package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class ClientUpdatePacket extends BasePacket<ClientUpdatePacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onClientUpdated(ClientUpdatePacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String networkId;
  private final Command command;
  private int inputTick;

  public ClientUpdatePacket(String networkId, Command command) {
    this.networkId = networkId;
    this.command = command;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Command getCommand() {
    return command;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  public int getInputTick() {
    return inputTick;
  }

  public void setInputTick(int inputTick) {
    this.inputTick = inputTick;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onClientUpdated(this, connection);
  }

}
