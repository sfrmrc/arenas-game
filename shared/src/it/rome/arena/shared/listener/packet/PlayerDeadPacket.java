package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class PlayerDeadPacket extends ServerMessage<PlayerDeadPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {}

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String networkId;

  public PlayerDeadPacket(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    throw new UnsupportedOperationException("PlayerDeadPacket is not supported");
  }

}
