package it.rome.arena.shared.listener.packet;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public enum Command {

  UP(0, 50), DOWN(0, -50), RIGHT(50, 0), LEFT(-50, 0), ACTION, NONE;

  float x, y;

  Command() {
    this(0, 0);
  }

  Command(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public void apply(Vector2 vector) {
    vector.x = x;
    vector.y = y;
  }

}
