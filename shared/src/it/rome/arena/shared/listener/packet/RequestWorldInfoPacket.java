package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class RequestWorldInfoPacket extends BasePacket<RequestWorldInfoPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onWorldInfoRequested(RequestWorldInfoPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onWorldInfoRequested(this, connection);
  }

}
