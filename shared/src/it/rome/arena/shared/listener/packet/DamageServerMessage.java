package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class DamageServerMessage extends ServerMessage<DamageServerMessage.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {}

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String networkId;
  private final Integer damage;

  public DamageServerMessage(String networkId, Integer damage) {
    super();
    this.networkId = networkId;
    this.damage = damage;
  }

  public String getNetworkId() {
    return networkId;
  }

  public Integer getDamage() {
    return damage;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    throw new UnsupportedOperationException("DamageServerMessage is not supported");
  }

}
