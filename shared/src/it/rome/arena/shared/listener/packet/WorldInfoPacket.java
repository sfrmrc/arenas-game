package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class WorldInfoPacket extends BasePacket<WorldInfoPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onWorldInfoReceived(WorldInfoPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final String worldId;
  private AddEntityPacket[] addEntities;

  public WorldInfoPacket(String worldId) {
    this.worldId = worldId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  public String getWorldId() {
    return worldId;
  }

  public void setAddEnities(AddEntityPacket[] addEntities) {
    this.addEntities = addEntities;
  }

  public AddEntityPacket[] getAddEntities() {
    return addEntities;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onWorldInfoReceived(this, connection);
  }


}
