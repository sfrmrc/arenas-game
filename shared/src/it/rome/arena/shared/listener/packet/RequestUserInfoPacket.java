package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RequestUserInfoPacket extends BasePacket<RequestUserInfoPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public interface Handler extends EventHandler {
    public void onUserInfoRequested(RequestUserInfoPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onUserInfoRequested(this, connection);
  }

}
