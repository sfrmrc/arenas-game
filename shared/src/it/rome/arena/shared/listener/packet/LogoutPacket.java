package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.UserId;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class LogoutPacket extends BasePacket<LogoutPacket.Handler> {

  public interface Handler extends EventHandler {}

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final UserId userId;

  public LogoutPacket(UserId userId) {
    super();
    this.userId = userId;
  }

  public UserId getUserId() {
    return userId;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {}

}
