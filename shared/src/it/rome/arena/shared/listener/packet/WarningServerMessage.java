package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class WarningServerMessage extends ServerMessage<WarningServerMessage.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onTextReceived(WarningServerMessage message);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private String text;

  public WarningServerMessage(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onTextReceived(this);
  }

}
