package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ServerMessagePacket extends BasePacket<ServerMessagePacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onMessageReceived(ServerMessagePacket packet);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  @SuppressWarnings("rawtypes")
  private ServerMessage[] messages;

  @SuppressWarnings("rawtypes")
  public void setMessages(ServerMessage[] messages) {
    this.messages = messages;
  }

  @SuppressWarnings("rawtypes")
  public ServerMessage[] getMessages() {
    return messages;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onMessageReceived(this);
  }

}
