package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class AddEntityPacket extends BasePacket<AddEntityPacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public interface Handler extends EventHandler {
    public void onEntityAdded(AddEntityPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final Shared shared;

  public AddEntityPacket(Shared shared) {
    this.shared = shared;
  }

  public String getNetworkId() {
    return shared.getNetworkId();
  }

  public Shared getShared() {
    return shared;
  }

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onEntityAdded(this, connection);
  }

}
