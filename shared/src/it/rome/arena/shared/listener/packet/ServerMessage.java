package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.listener.BasePacket;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public abstract class ServerMessage<H> extends BasePacket<H> {

}
