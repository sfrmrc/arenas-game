package it.rome.arena.shared.listener.packet;

import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.impl.PlayerShared;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

import java.util.Collection;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class CreatePlayerPacket extends BasePacket<CreatePlayerPacket.Handler> {

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  public interface Handler extends EventHandler {
    public void onCreatePlayer(CreatePlayerPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  private final Shared shared;
  private final Collection<Spell> spellBook;

  public CreatePlayerPacket(Shared shared, Collection<Spell> spellBook) {
    this.shared = shared;
    this.spellBook = spellBook;
  }

  public String getNetworkId() {
    return shared.getNetworkId();
  }

  public PlayerShared getShared() {
    return (PlayerShared) shared;
  }

  public Collection<Spell> getSpellBook() {
    return spellBook;
  }

  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onCreatePlayer(this, connection);
  }

}
