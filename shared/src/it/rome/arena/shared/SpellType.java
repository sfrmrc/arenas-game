package it.rome.arena.shared;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public enum SpellType {

  DIRECT, DOT, AREA, HOT

}
