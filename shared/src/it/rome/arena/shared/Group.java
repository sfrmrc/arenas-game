package it.rome.arena.shared;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class Group {

  public static final String CASTER = "caster";
  public static final String ENEMY = "enemy";
  public static final String ALLIES = "allies";
  public static final String GATES = "gates";
  public static final String OBSTACLES = "obstacles";
  public static final String CHECKPOINTS = "checkpoints";

}
