package it.rome.arena.shared;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public enum GameState {
  INITIALIZING, READY, FIGHTING, ENDED
}
