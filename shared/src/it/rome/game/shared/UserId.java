package it.rome.game.shared;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class UserId {

  private String username;

  public UserId(String username) {
    this.username = username;
  }

  public String getUsername() {
    return username;
  }

}
