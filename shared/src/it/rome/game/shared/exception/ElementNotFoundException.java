package it.rome.game.shared.exception;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ElementNotFoundException extends Exception {

  private static final long serialVersionUID = -5707839871280700609L;

  public ElementNotFoundException() {
    super();
  }

  public ElementNotFoundException(String message) {
    super(message);
  }

  public ElementNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public ElementNotFoundException(Throwable cause) {
    super(cause);
  }

}
