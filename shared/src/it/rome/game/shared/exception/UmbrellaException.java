package it.rome.game.shared.exception;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class UmbrellaException extends RuntimeException {

  private static final long serialVersionUID = -5885158883162380348L;

  private static final String MULTIPLE = " exceptions caught: ";

  private static final String ONE = "Exception caught: ";

  protected static Throwable makeCause(Set<Throwable> causes) {
    Iterator<Throwable> iterator = causes.iterator();
    if (!iterator.hasNext()) {
      return null;
    }

    return iterator.next();
  }

  protected static String makeMessage(Set<Throwable> causes) {
    int count = causes.size();
    if (count == 0) {
      return null;
    }

    StringBuilder b = new StringBuilder(count == 1 ? ONE : count + MULTIPLE);
    boolean first = true;
    for (Throwable t : causes) {
      if (first) {
        first = false;
      } else {
        b.append("; ");
      }
      b.append(t.getMessage());
    }

    return b.toString();
  }

  /**
   * The causes of the exception.
   */
  private Set<Throwable> causes;

  public UmbrellaException(Set<Throwable> causes) {
    super(makeMessage(causes), makeCause(causes));
    this.causes = causes;
  }

  /**
   * Required for GWT RPC serialization.
   */
  protected UmbrellaException() {
    // Can't delegate to the other constructor or GWT RPC gets cranky
    super(MULTIPLE);
    this.causes = Collections.<Throwable>emptySet();
  }

  /**
   * Get the set of exceptions that caused the failure.
   * 
   * @return the set of causes
   */
  public Set<Throwable> getCauses() {
    return causes;
  }
}
