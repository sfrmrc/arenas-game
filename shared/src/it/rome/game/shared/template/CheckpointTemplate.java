package it.rome.game.shared.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.component.SpatialComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.math.Ellipse;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class CheckpointTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    EllipseMapObject mapObject = parameters.get("mapObject");
    Ellipse ellipse = mapObject.getEllipse();
    Spatial spatial = new SpatialImpl(ellipse.x, ellipse.y, ellipse.width, ellipse.height);
    entityEdit.add(new SpatialComponent(spatial));
  }

}
