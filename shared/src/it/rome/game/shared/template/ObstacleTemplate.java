package it.rome.game.shared.template;

import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.builder.FixtureDefBuilder;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ObstacleTemplate extends EntityTemplate {

  private final BodyBuilder bodyBuilder;

  public ObstacleTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    RectangleMapObject mapObject = parameters.get("mapObject");
    String category = parameters.get("category", "-1");
    String mask = parameters.get("mask", "-1");

    if (mapObject == null) {
      throw new IllegalArgumentException("Object parameter cannot be null");
    }

    Rectangle rectangle = mapObject.getRectangle();
    convertRectangleInPhysicBox(rectangle);

    Spatial spatial = new SpatialImpl(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

    Integer type = parameters.get("type");
    BodyType bodyType = BodyType.StaticBody;
    if (type != null) {
      bodyType = BodyType.values()[type];
    }

    FixtureDefBuilder fixtureDefBuilder =
        bodyBuilder.fixtureDefBuilder().boxShape(rectangle.width, rectangle.height).density(1f);

    if (category != null && mask != null) {
      short shortM = Short.parseShort(mask);
      short shortC = Short.parseShort(category);
      fixtureDefBuilder.categoryBits(shortC).maskBits(shortM);
    }

    Body body =
        bodyBuilder.type(bodyType).userData(entity).position(spatial.getX(), spatial.getY())
            .fixture(fixtureDefBuilder).build();

    entityEdit.add(new SpatialComponent(new SpatialPhysicsImpl(body, spatial)));
    entityEdit.add(new PhysicsComponent(body));

  }

  private void convertRectangleInPhysicBox(Rectangle rectangle) {
    rectangle.x = rectangle.x + rectangle.width * 0.5f;
    rectangle.y = rectangle.y + rectangle.height * 0.5f;
    rectangle.width = rectangle.getWidth() * 0.5f;
    rectangle.height = rectangle.getHeight() * 0.5f;
  }

}
