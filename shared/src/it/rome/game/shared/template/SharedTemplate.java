package it.rome.game.shared.template;

import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.impl.BaseShared;
import it.rome.game.shared.component.SharedComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

public class SharedTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String networkId = parameters.get("networkId");

    if (networkId == null) {
      throw new IllegalArgumentException("NetworkId parameter cannot be null");
    }

    Shared shared = new BaseShared(networkId);

    entityEdit.add(new SharedComponent(shared));
  }

}
