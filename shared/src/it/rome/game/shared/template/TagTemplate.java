package it.rome.game.shared.template;

import it.rome.game.component.GroupComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class TagTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    String tag = parameters.get("tag");
    Object obj = parameters.get("group");

    EntityEdit entityEdit = entity.edit();

    if (!isEmpty(tag)) {
      entityEdit.add(new TagComponent(tag));
      entity.getWorld().getManager(TagManager.class).register(tag, entity);
    }

    if (!isEmpty(obj)) {
      if (obj instanceof String[]) {
        String[] group = (String[]) obj;
        entityEdit.add(new GroupComponent(group));
        for (String g : group) {
          entity.getWorld().getManager(GroupManager.class).add(entity, g);
        }
      } else if (obj instanceof String) {
        String group = (String) obj;
        entityEdit.add(new GroupComponent(group));
        entity.getWorld().getManager(GroupManager.class).add(entity, group);
      }
    }

  }

  private boolean isEmpty(Object tag) {
    return tag == null || "".equals(tag);
  }

}
