package it.rome.game.shared.template;

import it.rome.arena.shared.Group;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.template.map.TemplateGroups;

import com.badlogic.gdx.physics.box2d.World;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
// TODO: merge ArenaTemplateGroups with ServerGameObjectFactory and ArenaInstance
public class ArenaTemplateGroups extends TemplateGroups {

  public ArenaTemplateGroups(World physicWorld) {
    super();
    BodyBuilder bodyBuilder = new BodyBuilder(physicWorld);
    ObstacleTemplate obstacleTemplate = new ObstacleTemplate(bodyBuilder);
    SharedTemplate sharedTemplate = new SharedTemplate();
    TagTemplate tagTemplate = new TagTemplate();
    CheckpointTemplate checkpointTemplate = new CheckpointTemplate();
    add(Group.OBSTACLES, obstacleTemplate, tagTemplate);
    add(Group.CHECKPOINTS, checkpointTemplate, tagTemplate);
    add(Group.GATES, sharedTemplate, obstacleTemplate, tagTemplate);
  }

}
