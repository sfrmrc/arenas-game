package it.rome.game.shared;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public enum Channel {
  UDP, TCP
}
