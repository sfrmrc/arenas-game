package it.rome.game.shared;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class Arena {

  private final Integer id;
  private final String name;

  public Arena(Integer id, String name) {
    super();
    this.id = id;
    this.name = name;
  }
  
  public Integer getId() {
    return id;
  }
  
  public String getName() {
    return name;
  }

}
