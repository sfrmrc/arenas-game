package it.rome.game.shared.listener;

import it.rome.game.shared.listener.packet.ConnectionSucceededPacket;
import it.rome.game.shared.listener.packet.ConnectionTerminatedPacket;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class NetworkListener extends Listener {

  EventBus eventBus;

  public NetworkListener(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @Override
  public void connected(Connection connection) {
    ConnectionSucceededPacket connectionSucceededPacket = new ConnectionSucceededPacket();
    eventBus.fireEvent(connectionSucceededPacket, connection);
  }

  @Override
  public void disconnected(Connection connection) {
    ConnectionTerminatedPacket connectionTerminatedPacket = new ConnectionTerminatedPacket();
    eventBus.fireEvent(connectionTerminatedPacket, connection);
  }

  @Override
  public void received(Connection connection, Object obj) {
    if (!(obj instanceof BasePacket)) {
      return;
    }

    BasePacket<?> packet = (BasePacket<?>) obj;
    eventBus.fireEvent(packet, connection);

  }

}
