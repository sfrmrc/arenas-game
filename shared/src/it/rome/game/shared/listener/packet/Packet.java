package it.rome.game.shared.listener.packet;

import java.util.ArrayList;
import java.util.Collection;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class Packet {

  private final Collection<Class<?>> packages = new ArrayList<Class<?>>();

  public Packet(Class<?>... objs) {
    for (Class<?> clazz : objs) {
      packages.add(clazz);
    }
  }

  public void register(EndPoint endPoint) {
    Kryo kryo = endPoint.getKryo();
    for (Class<?> clazz : packages) {
      kryo.register(clazz);
    }
  }

}
