package it.rome.game.shared.listener.packet;

import com.esotericsoftware.kryonet.Connection;

import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.EventHandler;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class ConnectionTerminatedPacket extends BasePacket<ConnectionTerminatedPacket.Handler> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public interface Handler extends EventHandler {
    public void onConnectionTerminated(ConnectionTerminatedPacket packet, Connection connection);
  }

  public static final Type<Handler> TYPE = new Type<Handler>();

  @Override
  public Type<Handler> getType() {
    return TYPE;
  }

  @Override
  protected void dispatch(Handler handler, Connection connection) {
    handler.onConnectionTerminated(this, connection);
  }

}
