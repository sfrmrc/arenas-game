package it.rome.game.shared.listener;

import it.rome.game.shared.exception.UmbrellaException;
import it.rome.game.shared.listener.Event.Type;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class EventBus {

  private final Map<Event.Type<?>, List<?>> map = new HashMap<Event.Type<?>, List<?>>();

  public EventBus() {}

  public <H> HandlerRegistration addHandler(Type<H> type, H handler) {
    return doAdd(type, handler);
  }

  private <H> HandlerRegistration doAdd(final Event.Type<H> type, final H handler) {
    if (type == null) {
      throw new NullPointerException("Cannot add a handler with a null type");
    }
    if (handler == null) {
      throw new NullPointerException("Cannot add a null handler");
    }

    doAddNow(type, handler);

    return new HandlerRegistration() {
      public void removeHandler() {
        doRemove(type, handler);
      }
    };
  }

  private <H> void doAddNow(Event.Type<H> type, H handler) {
    List<H> l = ensureHandlerList(type);
    l.add(handler);
  }

  private <H> List<H> ensureHandlerList(Event.Type<H> type) {
    @SuppressWarnings("unchecked")
    List<H> handlers = (List<H>) map.get(type);
    if (handlers == null) {
      handlers = new ArrayList<H>();
      map.put(type, handlers);
    }
    return handlers;
  }

  private <H> void doRemove(Event.Type<H> type, H handler) {
    List<H> l = getHandlerList(type);
    boolean removed = l.remove(handler);
    if (removed && l.isEmpty()) {
      prune(type);
    }
  }

  private <H> List<H> getHandlerList(Event.Type<H> type) {
    @SuppressWarnings("unchecked")
    List<H> handlers = (List<H>) map.get(type);
    if (handlers == null) {
      return Collections.emptyList();
    }
    return handlers;
  }

  private void prune(Event.Type<?> type) {
    List<?> pruned = map.remove(type);
    assert pruned != null : "Can't prune what wasn't there";
    assert pruned.isEmpty() : "Pruned unempty list!";
  }

  public void fireEvent(Event<?> event, Connection connection) {
    doFire(event, connection);
  }

  private <H> void doFire(Event<H> event, Connection connection) {
    if (event == null) {
      throw new NullPointerException("Cannot fire null event");
    }

    List<H> handlers = getDispatchList(event.getType());
    Set<Throwable> causes = null;

    ListIterator<H> it = handlers.listIterator();
    while (it.hasNext()) {
      H handler = it.next();

      try {
        event.dispatch(handler, connection);
      } catch (Throwable e) {
        if (causes == null) {
          causes = new HashSet<Throwable>();
        }
        causes.add(e);
      }
    }

    if (causes != null) {
      throw new UmbrellaException(causes);
    }
  }

  private <H> List<H> getDispatchList(Event.Type<H> type) {
    List<H> directHandlers = getHandlerList(type);
    return directHandlers;
  }

}
