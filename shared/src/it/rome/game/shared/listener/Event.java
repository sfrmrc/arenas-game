package it.rome.game.shared.listener;

import com.esotericsoftware.kryonet.Connection;


/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public abstract class Event<H> {

  /**
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   * @param <H>
   */
  public static class Type<H> {}

  protected Event() {}

  public abstract Type<H> getType();
  
  protected abstract void dispatch(H handler, Connection connection);

}
