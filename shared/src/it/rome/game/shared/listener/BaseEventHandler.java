package it.rome.game.shared.listener;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public abstract class BaseEventHandler implements EventHandler {

  protected Connection connection;

}
