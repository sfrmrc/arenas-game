package it.rome.game.shared.listener;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public interface HandlerRegistration {

  void removeHandler();

}
