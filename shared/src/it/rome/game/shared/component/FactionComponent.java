package it.rome.game.shared.component;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class FactionComponent extends Component {

  final short faction;

  public FactionComponent(short faction) {
    super();
    this.faction = faction;
  }

  public short getFaction() {
    return faction;
  }

}
