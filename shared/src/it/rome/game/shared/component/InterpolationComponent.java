package it.rome.game.shared.component;

import it.rome.game.shared.attribute.Memento;

import com.artemis.Component;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * @since 1.0.0
 *
 */
public class InterpolationComponent extends Component {

  Memento current = new Memento();
  Memento old = new Memento();
  long lastUpdate;

  public Memento getCurrent() {
    return current;
  }

  public Memento getOld() {
    return old;
  }

  public long getLastUpdate() {
    return lastUpdate;
  }

  public void swap(Memento memento) {
    this.old.update(this.current);
    this.current = memento;
    lastUpdate = System.currentTimeMillis();
  }

}
