package it.rome.game.shared.component;

import it.rome.game.shared.attribute.Stats;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class StatsComponent extends Component {

  private final Stats stats;

  public StatsComponent(Stats stats) {
    this.stats = stats;
  }

  public Stats getStats() {
    return stats;
  }

}
