package it.rome.game.shared.component;

import it.rome.game.shared.attribute.Spell;

import java.util.Collection;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class WeaponComponent extends Component {

  private int actionId = -1;
  private final Collection<Spell> spellBook;

  public WeaponComponent(Collection<Spell> spellBook) {
    super();
    this.spellBook = spellBook;
  }

  public Collection<Spell> getSpellBook() {
    return spellBook;
  }

  public int getActionId() {
    return actionId;
  }

  public void setActionId(int actionId) {
    this.actionId = actionId;
  }

}
