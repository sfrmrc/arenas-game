package it.rome.game.shared.component;

import it.rome.game.shared.attribute.Shared;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class SharedComponent extends Component {

  final Shared shared;

  public SharedComponent(Shared shared) {
    this.shared = shared;
  }

  public Shared getShared() {
    return shared;
  }

}
