package it.rome.game.shared.attribute;

import com.badlogic.gdx.math.Vector2;


/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class Memento {

  int tick;
  float x, y, angle;
  Vector2 speed;

  public Memento() {}

  public Memento(int tick, float x, float y, float angle, Vector2 speed) {
    super();
    this.tick = tick;
    this.x = x;
    this.y = y;
    this.angle = angle;
    this.speed = speed;
  }

  public void update(Memento memento) {
    this.tick = memento.tick;
    this.x = memento.x;
    this.y = memento.y;
    this.angle = memento.angle;
    this.speed = memento.speed;
  }

  public int getTick() {
    return tick;
  }

  public float getX() {
    return x;
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }

  public float getAngle() {
    return angle;
  }

  public void setAngle(float angle) {
    this.angle = angle;
  }

  public void setTick(int tick) {
    this.tick = tick;
  }

  public Vector2 getSpeed() {
    return speed;
  }

  public void setSpeed(Vector2 speed) {
    this.speed = speed;
  }

}
