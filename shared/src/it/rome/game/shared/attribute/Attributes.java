package it.rome.game.shared.attribute;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class Attributes {

  /**
   * Attribute types
   * 
   * @author m.sferra
   * @since 0.0.1
   * 
   */
  public enum TYPE implements Serializable {
    STR, ITL, SPD, EVD, ACC, DEF, MDEF, VIT, LUK, RNG, CRIT, DRT, DMG, DOT, CASTING_TIME
  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   * @since 1.0.0
   *
   */
  public class Attribute implements Serializable {

    private static final long serialVersionUID = -7859720110071479299L;

    private final TYPE type;
    private int value = 1;

    public Attribute(TYPE type) {
      this.type = type;
    }

    public int getValue() {
      return value;
    }

    public TYPE getType() {
      return type;
    }

    public void setValue(int value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return type.toString() + ":" + value;
    }

  }

  private Map<TYPE, Attribute> attributes = new HashMap<TYPE, Attribute>();

  public Attributes() {
    for (TYPE type : TYPE.values()) {
      setAttribute(type, 0);
    }
  }

  public Attribute getAttribute(TYPE attribute) {
    return attributes.get(attribute);
  }

  protected void setAttribute(TYPE type, int value) {
    Attribute attribute = new Attribute(type);
    attribute.setValue(value);
    attributes.put(type, attribute);
  }

}
