package it.rome.game.shared.attribute;



/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public interface Shared {

  public String getNetworkId();

}
