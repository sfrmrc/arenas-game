package it.rome.game.shared.attribute.impl;

import it.rome.game.shared.attribute.Shared;

/**
 * 
 * @author marco
 *
 */
public class BaseShared implements Shared {

  private final String networkId;

  public BaseShared(String networkId) {
    super();
    this.networkId = networkId;
  }

  @Override
  public String getNetworkId() {
    return networkId;
  }

}
