package it.rome.game.shared.attribute.impl;

import it.rome.game.attribute.Spatial;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class MotionShared extends BaseShared {

  final Spatial spatial;
  final Vector2 speed;

  public MotionShared(String networkId, Spatial spatial, Vector2 speed) {
    super(networkId);
    this.spatial = spatial;
    this.speed = speed;
  }

  public Spatial getSpatial() {
    return spatial;
  }

  public Vector2 getSpeed() {
    return speed;
  }

}
