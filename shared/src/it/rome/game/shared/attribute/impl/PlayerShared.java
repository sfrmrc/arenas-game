package it.rome.game.shared.attribute.impl;

import it.rome.game.attribute.Spatial;
import it.rome.game.shared.attribute.Stats;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class PlayerShared extends MotionShared {

  final Stats stats;

  public PlayerShared(String networkId, Stats stats, Spatial spatial, Vector2 speed) {
    super(networkId, spatial, speed);
    this.stats = stats;
  }

  public Stats getStats() {
    return stats;
  }

}
