package it.rome.game.shared.attribute.impl;

import it.rome.game.attribute.Spatial;

import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class SpellShared extends MotionShared {

  final String casterId;
  final int commandId;

  public SpellShared(String networkId, String casterId, int commandId, Spatial spatial,
      Vector2 speed) {
    super(networkId, spatial, speed);
    this.casterId = casterId;
    this.commandId = commandId;
  }

  public String getCasterId() {
    return casterId;
  }

  public int getCommandId() {
    return commandId;
  }

}
