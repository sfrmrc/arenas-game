package it.rome.game.shared.attribute;

import it.rome.arena.shared.SpellCast;
import it.rome.arena.shared.SpellType;
import it.rome.game.shared.attribute.Attributes.TYPE;



/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class Spell {

  int id;
  SpellType type;
  SpellCast cast;
  Attributes attributes;

  Spell() {}

  public int getId() {
    return id;
  }

  public int getDamage() {
    return attributes.getAttribute(TYPE.DMG).getValue();
  }

  public int getRange() {
    return attributes.getAttribute(TYPE.RNG).getValue();
  }

  public int getCastingTime() {
    return attributes.getAttribute(TYPE.CASTING_TIME).getValue();
  }

  public Attributes getAttributes() {
    return attributes;
  }

  public SpellType getType() {
    return type;
  }

  public boolean isInstant() {
    return cast == SpellCast.INSTANT;
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public static class Builder {

    int id;
    SpellType type;
    SpellCast cast;
    Attributes attributes;

    public Builder() {
      clear();
    }

    private void clear() {
      attributes = new Attributes();
      id = -1;
      type = null;
      cast = SpellCast.INSTANT;
    }

    public Builder id(int id) {
      this.id = id;
      return this;
    }

    public Builder attr(TYPE type, int value) {
      attributes.setAttribute(type, value);
      return this;
    }

    public Attributes getAttributes() {
      return attributes;
    }

    public Builder damage(int value) {
      attributes.setAttribute(TYPE.DMG, value);
      return this;
    }

    public Builder range(int value) {
      attributes.setAttribute(TYPE.RNG, value);
      return this;
    }

    public Builder type(SpellType type) {
      this.type = type;
      return this;
    }

    public Builder cast(SpellCast cast) {
      this.cast = cast;
      return this;
    }

    public Spell build() {
      Spell spell = new Spell();
      spell.id = id;
      spell.attributes = attributes;
      spell.type = type;
      spell.cast = cast;
      clear();
      return spell;
    }
  }

}
