package it.rome.game.shared.attribute;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public enum PlayerState {
  W_UP, W_RIGHT, W_LEFT, W_DOWN, NOPE
}
