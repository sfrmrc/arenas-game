package it.rome.game.shared.attribute;

import it.rome.arena.shared.Class;
import it.rome.arena.shared.Race;
import it.rome.game.attribute.Health;
import it.rome.game.attribute.Points;
import it.rome.game.attribute.impl.HealthImpl;
import it.rome.game.attribute.impl.PointsImpl;
import it.rome.game.shared.attribute.Attributes.TYPE;

/**
 * Stores statistics
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class Stats {

  String name;
  Race race;
  Class clazz;
  Health health;
  Points points;
  short faction = 0x0001;
  Attributes attributes;

  Stats() {
    this.health = new HealthImpl(1);
    this.points = new PointsImpl(1);
  }

  public Health getHealth() {
    return health;
  }

  public Points getPoints() {
    return points;
  }

  public String getName() {
    return name;
  }

  public void setFaction(short faction) {
    this.faction = faction;
  }

  public short getFaction() {
    return faction;
  }

  public Race getRace() {
    return race;
  }
  
  public Class getClazz() {
    return clazz;
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  public static class Builder {

    String name;
    Race race;
    Class clazz;
    Short faction;
    Attributes attributes;
    Points points;
    Health health;

    public Builder() {
      clear();
    }

    private void clear() {
      this.attributes = new Attributes();
      this.name = "";
      this.faction = null;
      this.race = null;
      this.health = null;
      this.points = null;
      this.clazz = null;
    }

    public Builder name(String name) {
      this.name = name;
      return this;
    }

    public Builder faction(Short faction) {
      this.faction = faction;
      return this;
    }

    public Builder race(Race race) {
      this.race = race;
      return this;
    }

    public Builder attr(TYPE type, int value) {
      attributes.setAttribute(type, value);
      return this;
    }

    public Builder health(int health) {
      this.health = new HealthImpl(health);
      return this;
    }

    public Builder points(int points) {
      this.points = new PointsImpl(points);
      return this;
    }

    public Builder type(Class clazz) {
      this.clazz = clazz;
      return this;
    }

    public Stats build() {
      Stats stats = new Stats();
      stats.attributes = attributes;
      stats.name = name;
      stats.health = health != null ? health : new HealthImpl(100);
      stats.points = points != null ? points : new PointsImpl(100);
      stats.race = race;
      stats.faction = faction;
      stats.clazz = clazz;
      clear();
      return stats;
    }
  }

}
