package it.rome.game.shared;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class ClientInfo {

  protected UserId userId;
  protected int lastInputTick;

  public ClientInfo(UserId userId) {
    this(userId, 0);
  }

  public ClientInfo(UserId userId, int lastInputTick) {
    this.userId = userId;
    this.lastInputTick = lastInputTick;
  }

  public int getLastInputTick() {
    return lastInputTick;
  }

  public void setLastInputTick(int lastInputTick) {
    this.lastInputTick = lastInputTick;
  }

  public UserId getUserId() {
    return userId;
  }

}
