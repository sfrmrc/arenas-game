Game Framework
==============

Requirements
------------

- Java 1.8
- Gradle
- [LibGDX](https://github.com/libgdx/libgdx)
- Box2d
- [Overlap2D](https://github.com/UnderwaterApps/overlap2d-runtime-libgdx)
- Artemis
- Finite State Machine for Artemis

Status
------

![Status](https://codeship.com/projects/5ae16ea0-4415-0132-db48-72c9e56cf393/status?branch=master)
