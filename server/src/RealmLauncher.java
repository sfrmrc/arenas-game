import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.manager.CharacterManager;
import it.rome.arena.server.manager.UserManager;
import it.rome.arena.server.persistence.memory.MemoryStore;
import it.rome.arena.server.system.RealmNetworkingSystem;
import it.rome.arena.server.system.events.ServerArenaQueueSystem;
import it.rome.arena.server.system.events.ServerLoginSystem;
import it.rome.arena.server.system.events.ServerLogoutSystem;
import it.rome.arena.server.system.events.ServerUsersSystem;

import java.net.ConnectException;

import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.UuidEntityManager;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;


/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RealmLauncher extends ApplicationAdapter {

  MemoryStore memoryStore = new MemoryStore();
  UserManager userManager = new UserManager(memoryStore);
  CharacterManager characterManager = new CharacterManager(memoryStore);

  ArenaServer arenaServer = new ArenaServer();

  World world;

  @Override
  public void create() {
    super.create();
    Gdx.app.log("INFO", "REALM Server: Starting server ...");
    initWorld();
    try {
      long time = System.currentTimeMillis();
      arenaServer.start();
      Gdx.app.log("INFO", "Server: started in " + (System.currentTimeMillis() - time) + "ms");
      Gdx.app.log("INFO", "Server: listening on TCP:" + arenaServer.getTcpPort() + " - UDP:"
          + arenaServer.getUdpPort());
    } catch (ConnectException e) {
      e.printStackTrace();
      throw new RuntimeException(e.getCause());
    }
  }

  private void initWorld() {
    Gdx.app.log("INFO", "REALM Server: initializing systems ...");
    WorldConfiguration configuration = new WorldConfiguration();
    configuration.register(userManager).register(arenaServer).register(characterManager);
    world = new World(configuration);

    /* Managers */
    world.setManager(new TagManager());
    world.setManager(new GroupManager());
    world.setManager(new UuidEntityManager());

    /* Systems */
    world.setSystem(new RealmNetworkingSystem());
    world.setSystem(new ServerUsersSystem());
    world.setSystem(new ServerLoginSystem());
    world.setSystem(new ServerLogoutSystem());
    world.setSystem(new ServerArenaQueueSystem());

    world.initialize();
    Gdx.app.log("INFO", "REALM Server: world initialized");
  }

  @Override
  public void render() {
    super.render();
    world.setDelta(Gdx.graphics.getDeltaTime());
    world.process();
  }

  public static void main(String[] args) {
    HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
    cfg.renderInterval = 1 / 300f;
    new HeadlessApplication(new RealmLauncher(), cfg);
  }

}
