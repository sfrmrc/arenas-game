package it.rome.arena.server.component;

import it.rome.arena.server.event.LogoutServerEvent;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class LogoutEventComponent extends EventComponent<LogoutServerEvent> {

  public LogoutEventComponent(LogoutServerEvent event) {
    super(event);
  }

}
