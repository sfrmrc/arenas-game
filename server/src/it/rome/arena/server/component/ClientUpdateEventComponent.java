package it.rome.arena.server.component;

import it.rome.arena.server.event.ClientUpdateServerEvent;
import it.rome.game.shared.ClientInfo;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ClientUpdateEventComponent extends EventComponent<ClientUpdateServerEvent> {

  private final ClientInfo clientInfo;

  public ClientUpdateEventComponent(ClientUpdateServerEvent event, ClientInfo clientInfo) {
    super(event);
    this.clientInfo = clientInfo;
  }

  public ClientInfo getClientInfo() {
    return clientInfo;
  }

}
