package it.rome.arena.server.component;

import it.rome.game.shared.ClientInfo;

import com.artemis.Component;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class NetworkComponent extends Component {

  final ClientInfo clientInfo;
  final Connection connection;

  public NetworkComponent(ClientInfo clientInfo, Connection connection) {
    this.clientInfo = clientInfo;
    this.connection = connection;
  }

  public Connection getConnection() {
    return connection;
  }

  public String getNetworkId() {
    return clientInfo.getUserId().getUsername();
  }

  public int getLastInputTick() {
    return clientInfo.getLastInputTick();
  }

}
