package it.rome.arena.server.component;

import it.rome.arena.server.event.LoginSucceededServerEvent;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class LoginEventComponent extends EventComponent<LoginSucceededServerEvent> {

  public LoginEventComponent(LoginSucceededServerEvent event) {
    super(event);
  }

}
