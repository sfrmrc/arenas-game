package it.rome.arena.server.component;

import it.rome.arena.shared.listener.packet.ServerMessage;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ServerMessageComponent extends Component {

  final ServerMessage<?> message;

  public ServerMessageComponent(ServerMessage<?> message) {
    super();
    this.message = message;
  }

  public ServerMessage<?> getMessage() {
    return message;
  }

}
