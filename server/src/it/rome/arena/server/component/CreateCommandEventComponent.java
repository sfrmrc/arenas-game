package it.rome.arena.server.component;

import it.rome.arena.server.event.CreateCommandServerEvent;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class CreateCommandEventComponent extends EventComponent<CreateCommandServerEvent> {

  public CreateCommandEventComponent(CreateCommandServerEvent event) {
    super(event);
  }

}
