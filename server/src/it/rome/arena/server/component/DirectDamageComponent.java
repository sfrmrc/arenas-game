package it.rome.arena.server.component;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.game.attribute.EntityRef;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class DirectDamageComponent extends Component {

  private final SpellDamage spellDamage;
  private final EntityRef target;

  public DirectDamageComponent(SpellDamage spellDamage, EntityRef target) {
    super();
    this.spellDamage = spellDamage;
    this.target = target;
  }

  public SpellDamage getSpellDamage() {
    return spellDamage;
  }

  public EntityRef getTarget() {
    return target;
  }

}
