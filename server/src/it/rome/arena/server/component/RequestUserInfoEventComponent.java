package it.rome.arena.server.component;

import it.rome.arena.server.event.RequestUserInfoServerEvent;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class RequestUserInfoEventComponent extends EventComponent<RequestUserInfoServerEvent> {

  public RequestUserInfoEventComponent(RequestUserInfoServerEvent event) {
    super(event);
  }

}
