package it.rome.arena.server.component;

import it.rome.arena.server.event.ClientReadyServerEvent;
import it.rome.game.shared.ClientInfo;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ClientReadyEventComponent extends EventComponent<ClientReadyServerEvent> {

  private final ClientInfo clientInfo;

  public ClientReadyEventComponent(ClientInfo clientInfo, ClientReadyServerEvent event) {
    super(event);
    this.clientInfo = clientInfo;
  }

  public ClientInfo getClientInfo() {
    return clientInfo;
  }

}
