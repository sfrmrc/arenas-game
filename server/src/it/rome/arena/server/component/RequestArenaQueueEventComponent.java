package it.rome.arena.server.component;

import it.rome.arena.server.event.RequestArenaQueueServerEvent;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RequestArenaQueueEventComponent extends EventComponent<RequestArenaQueueServerEvent> {

  public RequestArenaQueueEventComponent(RequestArenaQueueServerEvent event) {
    super(event);
  }

}
