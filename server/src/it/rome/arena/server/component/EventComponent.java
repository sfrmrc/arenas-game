package it.rome.arena.server.component;

import it.rome.arena.server.event.ServerEvent;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public abstract class EventComponent<SE extends ServerEvent<?>> extends Component {

  protected final SE event;

  public EventComponent(SE event) {
    super();
    this.event = event;
  }

  public SE getEvent() {
    return event;
  }

}
