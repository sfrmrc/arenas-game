package it.rome.arena.server.component;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.game.attribute.EntityRef;
import it.rome.game.shared.attribute.Attributes.TYPE;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class DamageOverTimeComponent extends Component {

  private final SpellDamage spellDamage;
  private final EntityRef target;
  private final int tick;
  private float duration, life;
  private int exec;

  public DamageOverTimeComponent(SpellDamage spellDamage, EntityRef target) {
    super();
    this.spellDamage = spellDamage;
    this.target = target;
    this.tick = spellDamage.getSpell().getAttributes().getAttribute(TYPE.DOT).getValue();
    this.duration = spellDamage.getSpell().getAttributes().getAttribute(TYPE.DRT).getValue();
    this.exec = 0;
    this.life = 0;
  }

  public SpellDamage getSpellDamage() {
    return spellDamage;
  }

  public void update(float delta) {
    this.duration -= delta;
    this.life += delta;
    if (this.life >= tick) {
      this.life = 0;
      exec++;
    }
  }

  public int getExecute() {
    return exec;
  }

  public void decrementExecute(int i) {
    exec -= i;
  }

  public boolean isFinished() {
    return duration <= 0 && exec == 0;
  }

  public EntityRef getTarget() {
    return target;
  }

  public float getDuration() {
    return duration;
  }

  public int getExec() {
    return exec;
  }

  public float getLife() {
    return life;
  }

}
