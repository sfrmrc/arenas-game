package it.rome.arena.server.component;

import it.rome.arena.server.attribute.SpellDamage;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class SpellComponent extends Component {

  private final SpellDamage spellDamage;

  public SpellComponent(SpellDamage spellDamage) {
    this.spellDamage = spellDamage;
  }

  public SpellDamage getSpellDamage() {
    return spellDamage;
  }

}
