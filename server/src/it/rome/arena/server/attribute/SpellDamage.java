package it.rome.arena.server.attribute;

import it.rome.game.attribute.Health;
import it.rome.game.component.HealthComponent;
import it.rome.game.shared.attribute.Spell;

import com.artemis.Entity;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class SpellDamage {

  private final Spell spell;
  private final String caster;

  public SpellDamage(String caster, Spell spell) {
    super();
    this.caster = caster;
    this.spell = spell;
  }

  public int damage(Entity entity) {
    if (canHit(entity)) {
      HealthComponent healthComponent = entity.getComponent(HealthComponent.class);
      Health health = healthComponent.getHealth();
      health.decrease(spell.getDamage());
    }
    return spell.getDamage();
  }

  public boolean canHit(Entity entity) {
    return entity != null && entity.getComponent(HealthComponent.class) != null;
  }

  public String getCaster() {
    return caster;
  }

  public Spell getSpell() {
    return spell;
  }

}
