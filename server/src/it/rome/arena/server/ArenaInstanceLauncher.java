package it.rome.arena.server;

import it.rome.arena.server.manager.CharacterManager;
import it.rome.arena.server.manager.UserManager;
import it.rome.arena.server.persistence.SpellStore;
import it.rome.arena.server.persistence.memory.MemoryStore;
import it.rome.arena.server.system.CombatNetworkingSystem;
import it.rome.arena.server.system.CreationsSystem;
import it.rome.arena.server.system.DamageOverTimeSystem;
import it.rome.arena.server.system.DirectDamageSystem;
import it.rome.arena.server.system.DirectorSystem;
import it.rome.arena.server.system.ServerMessageDispatcher;
import it.rome.arena.server.system.ServerStatsDispatcher;
import it.rome.arena.server.system.ServerTickSystem;
import it.rome.arena.server.system.SpellCollisionSystem;
import it.rome.arena.server.system.events.ServerCreateCommandSystem;
import it.rome.arena.server.system.events.ServerLogoutSystem;
import it.rome.arena.server.system.events.ServerReadySystem;
import it.rome.arena.server.system.events.ServerUpdateSystem;
import it.rome.arena.shared.listener.packet.EndArenaQueuePacket;
import it.rome.game.shared.Channel;
import it.rome.game.shared.UserId;
import it.rome.game.system.PhysicsMovementSystem;
import it.rome.game.system.PhysicsSystem;
import it.rome.game.system.ScriptSystem;
import it.rome.game.utils.CollectionsUtil;

import java.util.Map;

import com.artemis.World;
import com.artemis.WorldConfiguration;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.managers.UuidEntityManager;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ArenaInstanceLauncher extends ApplicationAdapter {

  private static int count = 0;
  private final int index;

  private static final float TIME = 10;
  private static final String arenaName = "desert.tmx";

  private final ArenaServer arenaServer;
  private final MemoryStore memoryStore = new MemoryStore();
  private final CharacterManager characterManager = new CharacterManager(memoryStore);
  private final UserManager userManager = new UserManager(memoryStore);

  private final Map<UserId, Connection> clients;
  private World world;
  private com.badlogic.gdx.physics.box2d.World physicWorld;

  protected ArenaInstanceLauncher(ArenaServer arenaServer, Map<UserId, Connection> clients) {
    this.arenaServer = arenaServer;
    this.clients = CollectionsUtil.emptyMap();
    this.clients.putAll(clients);
    index = count++;
  }

  @Override
  public void create() {
    long time = System.currentTimeMillis();
    Gdx.app.log("INFO", "COMBAT Server" + index + ": Starting arena instance server ...");
    physicWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(), false);
    createArtemisWorld();
    long delta = System.currentTimeMillis() - time;
    Gdx.app.log("INFO", "COMBAT Server" + index + ": started in " + delta + "ms");
    int bodyCount = physicWorld.getBodyCount();
    Gdx.app.log("INFO", "COMBAT Server" + index + ": initialized world with " + bodyCount);
    notifyToClients(/* connections */);
  }

  private void notifyToClients() {
    EndArenaQueuePacket packet = new EndArenaQueuePacket(arenaName);
    arenaServer.send(Channel.TCP, packet);
  }

  private void createArtemisWorld() {

    WorldConfiguration configuration =
        new WorldConfiguration().register(arenaServer).register(characterManager)
            .register(SpellStore.class.getCanonicalName(), memoryStore).register(userManager);
    world = new World(configuration);

    /* Managers */
    world.setManager(new UuidEntityManager());
    world.setManager(new TagManager());
    world.setManager(new GroupManager());

    /* Systems */
    world.setSystem(new CreationsSystem(arenaName, physicWorld));

    world.setSystem(new CombatNetworkingSystem(clients));
    world.setSystem(new ServerLogoutSystem());
    world.setSystem(new ServerReadySystem());
    world.setSystem(new ServerUpdateSystem());
    world.setSystem(new ServerCreateCommandSystem());
    world.setSystem(new ServerTickSystem(arenaName, TIME));
    world.setSystem(new ServerStatsDispatcher(TIME));

    world.setSystem(new PhysicsSystem(physicWorld));
    world.setSystem(new PhysicsMovementSystem());
    world.setSystem(new SpellCollisionSystem());
    world.setSystem(new DamageOverTimeSystem());
    world.setSystem(new DirectDamageSystem());
    world.setSystem(new ServerMessageDispatcher());
    world.setSystem(new ScriptSystem());
    world.setSystem(new DirectorSystem());

    world.initialize();
    Gdx.app.log("INFO", "COMBAT Server: world initialized");

  }

  @Override
  public void render() {
    super.render();
    this.world.setDelta(Gdx.graphics.getDeltaTime());
    this.world.process();
  }

  @Override
  public void dispose() {
    super.dispose();
    long time = System.currentTimeMillis();
    Gdx.app.log("INFO", "COMBAT Server" + index + ": Shutting down Artemis world");
    world.dispose();
    long delta = System.currentTimeMillis() - time;
    Gdx.app.log("INFO", "COMBAT Server" + index + ": Shutdown " + delta + "ms");
  }

  public static void create(ArenaServer arenaServer, Map<UserId, Connection> clients) {
    HeadlessApplicationConfiguration cfg = new HeadlessApplicationConfiguration();
    cfg.renderInterval = 1 / 300f;
    ArenaInstanceLauncher arenaApplication = new ArenaInstanceLauncher(arenaServer, clients);
    new HeadlessApplication(arenaApplication, cfg);
  }

}
