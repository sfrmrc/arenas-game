package it.rome.arena.server.manager;

import it.rome.arena.server.exception.CharacterNotFoundException;
import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.server.persistence.CharacterStore;
import it.rome.game.shared.attribute.Stats;

/**
 * 
 * @author marco
 * @since 0.0.1
 * @since 1.0.0
 *
 */
public class CharacterManager {

  private final CharacterStore characterStore;

  public CharacterManager(CharacterStore characterStore) {
    this.characterStore = characterStore;
  }

  public Stats findStats(String character) throws CharacterNotFoundException {
    Stats stats;
    try {
      stats = characterStore.getStats(character);
      if (stats == null) {
        throw new CharacterNotFoundException();
      }
      return stats;
    } catch (PersistenceException e) {
      throw new CharacterNotFoundException(e);
    }
  }

}
