package it.rome.arena.server.manager;

import it.rome.game.shared.Arena;
import it.rome.game.shared.exception.ElementNotFoundException;
import it.rome.game.utils.CollectionsUtil;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ArenaManager {

  private static final Map<String, Arena> arenas = CollectionsUtil.emptyMap();

  static {
    Arena desert = new Arena(1, "desert.tmx");
    arenas.put("desert", desert);
  }

  public Arena find(String arenaName) throws ElementNotFoundException {
    Arena arena = arenas.get(arenaName);
    if (arena == null) {
      throw new ElementNotFoundException("Arena " + arenaName + " not found");
    }
    return arena;
  }

  public List<Arena> findAll() {
    return CollectionsUtil.toList(arenas.values());
  }

}
