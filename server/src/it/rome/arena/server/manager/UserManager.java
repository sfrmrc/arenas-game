package it.rome.arena.server.manager;

import it.rome.arena.server.exception.AuthenticationException;
import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.server.exception.UserIdNotFoundException;
import it.rome.arena.server.persistence.UserStore;
import it.rome.game.shared.UserId;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class UserManager {

  private static final Set<String> loggedin = new HashSet<String>();

  private final UserStore userStore;

  public UserManager(UserStore userStore) {
    this.userStore = userStore;
  }

  public UserId findUserId(String username) throws UserIdNotFoundException {
    UserId userId;
    try {
      userId = userStore.getUserId(username);
      if (userId == null) {
        throw new UserIdNotFoundException();
      }
      return userId;
    } catch (PersistenceException e) {
      throw new UserIdNotFoundException(e);
    }
  }

  public boolean isAuthenticated(String username) throws AuthenticationException {
    return loggedin.contains(username);
  }

  public UserId authenticate(String username, String password) throws AuthenticationException {
    if (isAuthenticated(username)) {
      throw new AuthenticationException("User already logged");
    }
    UserId userId;
    try {
      userId = userStore.getUserId(username);
      if (userId != null) {
        loggedin.add(username);
        return userId;
      }
    } catch (PersistenceException e) {
      throw new AuthenticationException(e);
    }
    throw new AuthenticationException("User not found");
  }

  public void disconnect(UserId userId) throws AuthenticationException {
    if (!isAuthenticated(userId.getUsername())) {
      throw new AuthenticationException();
    }
    loggedin.remove(userId.getUsername());
  }

}
