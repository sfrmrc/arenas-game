package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.LogoutPacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class LogoutServerEvent extends ServerEvent<LogoutPacket> {

  public LogoutServerEvent(Connection connection, LogoutPacket packet) {
    super(connection, packet);
  }

}
