package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.RequestUserInfoPacket;
import it.rome.game.shared.UserId;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RequestUserInfoServerEvent extends ServerEvent<RequestUserInfoPacket> {

  private final UserId userId;

  public RequestUserInfoServerEvent(UserId userId, Connection connection,
      RequestUserInfoPacket packet) {
    super(connection, packet);
    this.userId = userId;
  }

  public UserId getUserId() {
    return userId;
  }

}
