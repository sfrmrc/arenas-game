package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.ClientUpdatePacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ClientUpdateServerEvent extends ServerEvent<ClientUpdatePacket> {

  public ClientUpdateServerEvent(Connection connection, ClientUpdatePacket packet) {
    super(connection, packet);
  }

}
