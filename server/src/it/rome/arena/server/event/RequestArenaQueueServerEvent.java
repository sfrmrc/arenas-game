package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.RequestArenaQueuePacket;
import it.rome.game.shared.UserId;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class RequestArenaQueueServerEvent extends ServerEvent<RequestArenaQueuePacket> {

  private final UserId userId;

  public RequestArenaQueueServerEvent(UserId userId, Connection connection,
      RequestArenaQueuePacket packet) {
    super(connection, packet);
    this.userId = userId;
  }

  public UserId getUserId() {
    return userId;
  }

}
