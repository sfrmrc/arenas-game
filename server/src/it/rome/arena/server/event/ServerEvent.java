package it.rome.arena.server.event;

import it.rome.game.shared.listener.BasePacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ServerEvent<P extends BasePacket<?>> {

  private final Connection connection;
  private final P packet;

  public ServerEvent(Connection connection, P packet) {
    this.connection = connection;
    this.packet = packet;
  }

  public Connection getConnection() {
    return connection;
  }

  public P getPacket() {
    return packet;
  }

}
