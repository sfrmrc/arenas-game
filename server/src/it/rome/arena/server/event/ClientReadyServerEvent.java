package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.ClientReadyPacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class ClientReadyServerEvent extends ServerEvent<ClientReadyPacket> {

  public ClientReadyServerEvent(Connection connection, ClientReadyPacket packet) {
    super(connection, packet);
  }

}
