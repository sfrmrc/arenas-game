package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.CreateCommandEntityPacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class CreateCommandServerEvent extends ServerEvent<CreateCommandEntityPacket> {

  public CreateCommandServerEvent(Connection connection, CreateCommandEntityPacket packet) {
    super(connection, packet);
  }

}
