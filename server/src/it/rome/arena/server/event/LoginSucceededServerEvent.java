package it.rome.arena.server.event;

import it.rome.arena.shared.listener.packet.LoginSucceededPacket;

import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class LoginSucceededServerEvent extends ServerEvent<LoginSucceededPacket> {

  public LoginSucceededServerEvent(Connection connection, LoginSucceededPacket packet) {
    super(connection, packet);
  }

}
