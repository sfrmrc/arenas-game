package it.rome.arena.server.system.events;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.LoginEventComponent;
import it.rome.arena.server.event.LoginSucceededServerEvent;
import it.rome.arena.shared.listener.packet.LoginSucceededPacket;
import it.rome.game.shared.Channel;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerLoginSystem extends EntityProcessingSystem {

  ComponentMapper<LoginEventComponent> lem;

  @Wire
  ArenaServer arenaServer;

  @SuppressWarnings("unchecked")
  public ServerLoginSystem() {
    super(Aspect.getAspectForAll(LoginEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    LoginEventComponent loginEventComponent = lem.get(entity);
    LoginSucceededServerEvent event = loginEventComponent.getEvent();
    LoginSucceededPacket packet = event.getPacket();
    Connection connection = event.getConnection();
    if (connection.isConnected()) {
      arenaServer.send(Channel.TCP, connection, packet);
    }
    entity.deleteFromWorld();
  }

}
