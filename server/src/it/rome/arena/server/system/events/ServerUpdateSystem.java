package it.rome.arena.server.system.events;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.ClientUpdateEventComponent;
import it.rome.arena.shared.listener.packet.ClientUpdatePacket;
import it.rome.arena.shared.listener.packet.Command;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.ClientInfo;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerUpdateSystem extends EntityProcessingSystem {

  ComponentMapper<ClientUpdateEventComponent> cuem;
  ComponentMapper<VelocityComponent> vm;
  TagManager tagManager;
  
  @Wire
  ArenaServer arenaServer;

  @SuppressWarnings("unchecked")
  public ServerUpdateSystem() {
    super(Aspect.getAspectForAll(ClientUpdateEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    ClientUpdateEventComponent clientUpdateEventComponent = cuem.get(entity);
    ClientUpdatePacket packet = clientUpdateEventComponent.getEvent().getPacket();
    ClientInfo clientInfo = clientUpdateEventComponent.getClientInfo();
    clientInfo.setLastInputTick(packet.getInputTick());
    updateClient(packet);
    entity.deleteFromWorld();
  }

  // FIXME: apply command at the same tick!!!
  private void updateClient(ClientUpdatePacket packet) {
    Entity entity = tagManager.getEntity(packet.getNetworkId());
    if (entity == null || !vm.has(entity)) {
      return;
    }
    Command command = packet.getCommand();
    VelocityComponent velocityComponent = vm.get(entity);
    if (velocityComponent != null) {
      command.apply(velocityComponent.getSpeed());
    }
  }

}
