package it.rome.arena.server.system.events;

import it.rome.arena.server.ArenaInstanceLauncher;
import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.RequestArenaQueueEventComponent;
import it.rome.arena.server.system.ServerNetworkingSystem;
import it.rome.arena.shared.listener.packet.BeginArenaQueue;
import it.rome.game.shared.Channel;
import it.rome.game.shared.UserId;
import it.rome.game.utils.CollectionsUtil;

import java.util.Map;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Wire;
import com.artemis.utils.ImmutableBag;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerArenaQueueSystem extends EntitySystem {

  ComponentMapper<RequestArenaQueueEventComponent> raqm;

  ServerNetworkingSystem serverNetworkingSystem;

  @Wire
  ArenaServer arenaServer;

  Map<UserId, Connection> clients = CollectionsUtil.emptyMap();

  @SuppressWarnings("unchecked")
  public ServerArenaQueueSystem() {
    super(Aspect.getAspectForAll(RequestArenaQueueEventComponent.class));
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {
    for (Entity entity : entities) {
      RequestArenaQueueEventComponent requestArenaQueueEventComponent = raqm.get(entity);
      Connection connection = requestArenaQueueEventComponent.getEvent().getConnection();
      UserId userId = requestArenaQueueEventComponent.getEvent().getUserId();
      if (userId != null) {
        clients.put(userId, connection);
        BeginArenaQueue packet = new BeginArenaQueue();
        arenaServer.send(Channel.TCP, connection, packet);
      }
      entity.deleteFromWorld();
    }
  }

  // TODO: change this ...
  @Override
  protected void end() {
    super.end();
    if (clients.size() >= 2) {
      ArenaInstanceLauncher.create(arenaServer, clients);
      clients.clear();
    }
  }

}
