package it.rome.arena.server.system.events;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.RequestUserInfoEventComponent;
import it.rome.arena.server.event.RequestUserInfoServerEvent;
import it.rome.arena.server.exception.CharacterNotFoundException;
import it.rome.arena.server.manager.CharacterManager;
import it.rome.arena.shared.listener.packet.OperationFailedPacket;
import it.rome.arena.shared.listener.packet.UserInfoPacket;
import it.rome.game.shared.Channel;
import it.rome.game.shared.UserId;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.utils.CollectionsUtil;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerUsersSystem extends EntityProcessingSystem {

  ComponentMapper<RequestUserInfoEventComponent> ruim;

  @Wire
  ArenaServer arenaServer;

  @Wire
  CharacterManager characterManager;

  @SuppressWarnings("unchecked")
  public ServerUsersSystem() {
    super(Aspect.getAspectForAll(RequestUserInfoEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    RequestUserInfoEventComponent requestUserInfoEventComponent = ruim.get(entity);
    RequestUserInfoServerEvent event = requestUserInfoEventComponent.getEvent();
    Connection connection = event.getConnection();
    try {
      UserId userId = event.getUserId();
      Stats stats = characterManager.findStats(userId.getUsername());
      Stats[] statsArray = CollectionsUtil.toList(stats).toArray(new Stats[1]);
      UserInfoPacket packet = new UserInfoPacket(statsArray);
      arenaServer.send(Channel.TCP, connection, packet);
    } catch (CharacterNotFoundException e) {
      e.printStackTrace();
      Gdx.app.log("ERROR", "REALM Server: error reading user's characters");
      OperationFailedPacket packet = new OperationFailedPacket(e.getMessage());
      arenaServer.send(Channel.TCP, connection, packet);
    }
    entity.deleteFromWorld();
  }

}
