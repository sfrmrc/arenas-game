package it.rome.arena.server.system.events;

import it.rome.arena.server.component.LogoutEventComponent;
import it.rome.arena.server.event.LogoutServerEvent;
import it.rome.arena.server.exception.AuthenticationException;
import it.rome.arena.server.manager.UserManager;
import it.rome.arena.shared.listener.packet.LogoutPacket;
import it.rome.game.shared.UserId;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerLogoutSystem extends EntityProcessingSystem {

  ComponentMapper<LogoutEventComponent> lem;
  TagManager tagManager;

  @Wire
  UserManager userManager;

  @SuppressWarnings("unchecked")
  public ServerLogoutSystem() {
    super(Aspect.getAspectForAll(LogoutEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    LogoutEventComponent logoutEventComponent = lem.get(entity);
    LogoutServerEvent event = logoutEventComponent.getEvent();
    LogoutPacket packet = event.getPacket();
    Connection connection = event.getConnection();
    if (!connection.isConnected()) {
      try {
        UserId userId = packet.getUserId();
        Entity toDelete = tagManager.getEntity(userId.getUsername());
        if (toDelete == null) {
          Gdx.app.log("ERROR", "Server: error removing entity " + toDelete);
        } else {
          toDelete.deleteFromWorld();
        }
        userManager.disconnect(userId);
      } catch (AuthenticationException e) {
        Gdx.app.log("ERROR", "Server: error disconnecting auth");
      }
    }
    entity.deleteFromWorld();
  }

}
