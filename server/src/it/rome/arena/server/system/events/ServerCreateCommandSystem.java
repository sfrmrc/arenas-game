package it.rome.arena.server.system.events;

import it.rome.arena.server.component.CreateCommandEventComponent;
import it.rome.arena.server.event.CreateCommandServerEvent;
import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.server.persistence.SpellStore;
import it.rome.arena.server.system.CreationsSystem;
import it.rome.arena.shared.listener.packet.CreateCommandEntityPacket;
import it.rome.arena.shared.listener.packet.WarningServerMessage;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.component.WeaponComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerCreateCommandSystem extends EntityProcessingSystem {

  ComponentMapper<CreateCommandEventComponent> ccem;
  ComponentMapper<WeaponComponent> wm;
  ComponentMapper<SpatialComponent> sm;
  ComponentMapper<VelocityComponent> vm;

  CreationsSystem creationsSystem;

  TagManager tagManager;

  @Wire
  SpellStore spellStore;

  @SuppressWarnings("unchecked")
  public ServerCreateCommandSystem() {
    super(Aspect.getAspectForAll(CreateCommandEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    CreateCommandEventComponent createCommandEventComponent = ccem.get(entity);
    CreateCommandServerEvent event = createCommandEventComponent.getEvent();
    CreateCommandEntityPacket packet = event.getPacket();
    int commandId = packet.getCommandId();
    try {
      Spell spell = spellStore.getSpell(commandId);
      if (canCastSpell(packet, spell)) {
        createCommand(packet, spell);
      } else {
        WarningServerMessage message = new WarningServerMessage("Cannot cast spell while moving");
        creationsSystem.createMessage(message);
      }
    } catch (PersistenceException e) {
      Gdx.app.log("ERROR", "Server: error creating spell from command " + commandId, e);
    }

    entity.deleteFromWorld();
  }

  private boolean canCastSpell(CreateCommandEntityPacket packet, Spell spell) {
    Entity entity = tagManager.getEntity(packet.getNetworkId());
    if (!spell.isInstant() && vm.has(entity)) {
      return Vector2.Zero.equals(vm.get(entity).getSpeed());
    }
    return true;
  }

  // FIXME: apply command at the same tick!!!
  private void createCommand(CreateCommandEntityPacket packet, Spell spell) {
    Entity entity = tagManager.getEntity(packet.getNetworkId());
    if (wm.has(entity)) {
      Spatial spatial = sm.get(entity).getSpatial();
      creationsSystem.createSpell(entity, spell, packet.getCommandId(), spatial.getX(),
          spatial.getY(), packet.getX(), packet.getY());
    }
  }

}
