package it.rome.arena.server.system.events;

import it.rome.arena.server.component.ClientReadyEventComponent;
import it.rome.arena.server.event.ClientReadyServerEvent;
import it.rome.arena.server.exception.CharacterNotFoundException;
import it.rome.arena.server.manager.CharacterManager;
import it.rome.arena.server.system.CreationsSystem;
import it.rome.arena.shared.listener.packet.ClientReadyPacket;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.attribute.Stats;

import java.util.UUID;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class ServerReadySystem extends EntityProcessingSystem {

  ComponentMapper<ClientReadyEventComponent> crem;

  CreationsSystem creationsSystem;

  @Wire
  CharacterManager characterManager;

  @SuppressWarnings("unchecked")
  public ServerReadySystem() {
    super(Aspect.getAspectForAll(ClientReadyEventComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    ClientReadyEventComponent clientReadyEventComponent = crem.get(entity);
    ClientReadyServerEvent clientReadyServerEvent = clientReadyEventComponent.getEvent();
    ClientReadyPacket clientReadyPacket = clientReadyServerEvent.getPacket();
    Connection connection = clientReadyServerEvent.getConnection();
    String networkId = clientReadyPacket.getNetworkId();
    try {
      Stats stats = characterManager.findStats(networkId);
      ClientInfo clientInfo = clientReadyEventComponent.getClientInfo();
      UUID uuid = creationsSystem.createPlayer(clientInfo, connection, networkId, stats);
      Gdx.app.log("DEBUG", "Server: player created with id: " + uuid);
    } catch (CharacterNotFoundException e) {
      Gdx.app.log("ERROR", "Server: error creating player");
    }
    entity.deleteFromWorld();
  }

}
