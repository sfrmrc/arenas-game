package it.rome.arena.server.system;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.component.DirectDamageComponent;
import it.rome.arena.shared.listener.packet.DamageServerMessage;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.TagComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
// Spell damages entities on impacts
// Spell on area?
@Wire
public class DirectDamageSystem extends EntityProcessingSystem {

  ComponentMapper<HealthComponent> hm;
  ComponentMapper<TagComponent> tm;
  ComponentMapper<DirectDamageComponent> sm;

  CreationsSystem creationsSystem;

  @SuppressWarnings("unchecked")
  public DirectDamageSystem() {
    super(Aspect.getAspectForAll(DirectDamageComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    DirectDamageComponent spellDamageComponent = sm.get(entity);
    SpellDamage spellDamage = spellDamageComponent.getSpellDamage();
    if (spellDamageComponent.getTarget() != null) {
      Entity targetEnt = spellDamageComponent.getTarget().getEntity();
      if (targetEnt != null && tm.has(targetEnt)) {
        TagComponent tagComponent = tm.get(targetEnt);
        String target = tagComponent.getTag();
        inflictDamage(spellDamage, targetEnt, target);
      }
      entity.deleteFromWorld();
    }
  }

  private void inflictDamage(SpellDamage spellDamage, Entity targetEnt, String target) {
    if (spellDamage.canHit(targetEnt)) {
      int damage = spellDamage.damage(targetEnt);
      DamageServerMessage message = new DamageServerMessage(target, damage);
      creationsSystem.createMessage(message);
    }
  }

}
