package it.rome.arena.server.system;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.NetworkComponent;
import it.rome.arena.server.packet.utils.ServerPacketUtils;
import it.rome.arena.server.persistence.SpellStore;
import it.rome.arena.shared.listener.packet.CreatePlayerPacket;
import it.rome.arena.shared.listener.packet.RemoveEntityPacket;
import it.rome.arena.shared.listener.packet.ServerUpdatePacket;
import it.rome.arena.shared.listener.packet.WorldInfoPacket;
import it.rome.game.shared.Channel;
import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.component.SharedComponent;
import it.rome.game.shared.component.WeaponComponent;

import java.util.ArrayList;
import java.util.Collection;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
@Wire
public class ServerTickSystem extends IntervalEntitySystem {

  ComponentMapper<SharedComponent> sm;
  ComponentMapper<WeaponComponent> wm;
  ComponentMapper<NetworkComponent> nm;

  @Wire
  ArenaServer arenaServer;

  @Wire
  SpellStore spellStore;

  final String arenaName;
  int tick = 0;
  WorldInfoPacket worldInfoPacket;

  @SuppressWarnings("unchecked")
  public ServerTickSystem(String arenaName, float interval) {
    super(Aspect.getAspectForAll(SharedComponent.class), interval / 1000);
    this.arenaName = arenaName;
  }

  @Override
  protected void begin() {
    worldInfoPacket = ServerPacketUtils.createWorldSnapshot(arenaName, getActives());
  }

  @Override
  protected void inserted(Entity entity) {
    super.inserted(entity);
    SharedComponent sharedComponent = sm.get(entity);
    Shared shared = sharedComponent.getShared();
    Gdx.app.log("DEBUG", "Server: inserting entity " + shared.getNetworkId());
    if (nm.has(entity)) {
      createPlayer(entity);
    }
    arenaServer.send(Channel.TCP, ServerPacketUtils.createAddEntityPacket(entity));
  }

  @Override
  protected void removed(Entity entity) {
    SharedComponent sharedComponent = sm.get(entity);
    Shared shared = sharedComponent.getShared();
    Gdx.app.log("DEBUG", "Server: removing entity " + shared.getNetworkId());
    RemoveEntityPacket removeEntityPacket = new RemoveEntityPacket(shared.getNetworkId());
    arenaServer.send(Channel.TCP, removeEntityPacket);
    super.removed(entity);
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {
    if (entities.isEmpty()) {
      return;
    }
    tick++;
    ServerUpdatePacket serverUpdatePacket = new ServerUpdatePacket();
    serverUpdatePacket.setEntityUpdates(ServerPacketUtils.createWorldUpdate(entities));
    serverUpdatePacket.setTickNum(tick);
    for (Entity entity : entities) {
      if (nm.has(entity)) {
        NetworkComponent networkComponent = nm.get(entity);
        serverUpdatePacket.setPlayerInputTick(networkComponent.getLastInputTick());
        arenaServer.send(Channel.UDP, networkComponent.getConnection(), serverUpdatePacket);
      }
    }
  }

  private void createPlayer(Entity entity) {
    NetworkComponent networkComponent = nm.get(entity);
    CreatePlayerPacket createPlayerPacket = createPlayerPacket(entity);
    Connection connection = networkComponent.getConnection();
    arenaServer.send(Channel.TCP, connection, createPlayerPacket);
    arenaServer.send(Channel.TCP, connection, worldInfoPacket);
  }

  private CreatePlayerPacket createPlayerPacket(Entity entity) {
    Collection<Spell> spellBook;
    if (wm.has(entity)) {
      WeaponComponent weaponComponent = wm.get(entity);
      spellBook = weaponComponent.getSpellBook();
    } else {
      spellBook = new ArrayList<Spell>();
    }
    return ServerPacketUtils.createNewPlayerPacket(entity, spellBook);
  }

}
