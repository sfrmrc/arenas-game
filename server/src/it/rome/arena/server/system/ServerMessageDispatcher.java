package it.rome.arena.server.system;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.ServerMessageComponent;
import it.rome.arena.shared.listener.packet.ServerMessage;
import it.rome.arena.shared.listener.packet.ServerMessagePacket;
import it.rome.game.shared.Channel;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Wire;
import com.artemis.utils.ImmutableBag;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
@Wire
public class ServerMessageDispatcher extends EntitySystem {

  ComponentMapper<ServerMessageComponent> sm;

  @Wire
  ArenaServer arenaServer;

  @SuppressWarnings("unchecked")
  public ServerMessageDispatcher() {
    super(Aspect.getAspectForAll(ServerMessageComponent.class));
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {
    ServerMessagePacket serverMessagePacket = new ServerMessagePacket();
    ServerMessage<?>[] messages = new ServerMessage[entities.size()];
    for (int i = 0; i < entities.size(); i++) {
      Entity entity = entities.get(i);
      ServerMessageComponent m = sm.get(entity);
      messages[i] = m.getMessage();
    }
    serverMessagePacket.setMessages(messages);
    arenaServer.send(Channel.TCP, serverMessagePacket);
  }

  @Override
  protected void end() {
    super.end();
    for (Entity entity : getActives()) {
      entity.deleteFromWorld();
    }
  }

}
