package it.rome.arena.server.system;

import it.rome.arena.server.component.ClientReadyEventComponent;
import it.rome.arena.server.component.ClientUpdateEventComponent;
import it.rome.arena.server.component.CreateCommandEventComponent;
import it.rome.arena.server.event.ClientReadyServerEvent;
import it.rome.arena.server.event.ClientUpdateServerEvent;
import it.rome.arena.server.event.CreateCommandServerEvent;
import it.rome.arena.shared.listener.packet.ClientReadyPacket;
import it.rome.arena.shared.listener.packet.ClientUpdatePacket;
import it.rome.arena.shared.listener.packet.CreateCommandEntityPacket;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.UserId;
import it.rome.game.shared.listener.packet.ConnectionTerminatedPacket;

import java.util.Map;

import com.artemis.Component;
import com.artemis.annotations.Wire;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire(injectInherited = true)
public class CombatNetworkingSystem extends ServerNetworkingSystem {

  public CombatNetworkingSystem(Map<UserId, Connection> clients) {
    super();
    for (UserId userId : clients.keySet()) {
      ClientInfo clientInfo = new ClientInfo(userId);
      Connection connection = clients.get(userId);
      this.clients.put(connection, clientInfo);
    }
  }

  @Override
  protected void initialize() {
    arenaServer.addHandler(ConnectionTerminatedPacket.TYPE, new ConnectionTerminatedListener());
    arenaServer.addHandler(ClientUpdatePacket.TYPE, new ClientUpdateListener());
    arenaServer.addHandler(CreateCommandEntityPacket.TYPE, new CreateCommandEntityListener());
    arenaServer.addHandler(ClientReadyPacket.TYPE, new ClientReadyListener());
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class ClientUpdateListener implements ClientUpdatePacket.Handler {

    @Override
    public void onClientUpdated(ClientUpdatePacket packet, Connection connection) {
      ClientUpdateServerEvent serverEvent = new ClientUpdateServerEvent(connection, packet);
      ClientInfo clientInfo = clients.get(connection);
      Component component = new ClientUpdateEventComponent(serverEvent, clientInfo);
      addServerEvent(component);
    }

  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class CreateCommandEntityListener implements CreateCommandEntityPacket.Handler {

    @Override
    public void onCommandEntityReceived(CreateCommandEntityPacket packet, Connection connection) {
      CreateCommandServerEvent serverEvent = new CreateCommandServerEvent(connection, packet);
      CreateCommandEventComponent component = new CreateCommandEventComponent(serverEvent);
      addServerEvent(component);
    }

  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class ClientReadyListener implements ClientReadyPacket.Handler {

    @Override
    public void onClientReady(ClientReadyPacket packet, Connection connection) {
      ClientReadyServerEvent serverEvent = new ClientReadyServerEvent(connection, packet);
      ClientInfo clientInfo = clients.get(connection);
      Component component = new ClientReadyEventComponent(clientInfo, serverEvent);
      addServerEvent(component);
    }

  }

}
