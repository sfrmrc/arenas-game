package it.rome.arena.server.system;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.component.LoginEventComponent;
import it.rome.arena.server.component.LogoutEventComponent;
import it.rome.arena.server.event.LoginSucceededServerEvent;
import it.rome.arena.server.event.LogoutServerEvent;
import it.rome.arena.server.exception.AuthenticationException;
import it.rome.arena.server.exception.UserIdNotFoundException;
import it.rome.arena.server.manager.UserManager;
import it.rome.arena.shared.listener.packet.LoginPacket;
import it.rome.arena.shared.listener.packet.LoginSucceededPacket;
import it.rome.arena.shared.listener.packet.LogoutPacket;
import it.rome.arena.shared.listener.packet.OperationFailedPacket;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.Channel;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.UserId;
import it.rome.game.shared.listener.packet.ConnectionTerminatedPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.artemis.Component;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.annotations.Wire;
import com.artemis.managers.TagManager;
import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
@Wire
public abstract class ServerNetworkingSystem extends VoidEntitySystem {

  ComponentMapper<VelocityComponent> vm;
  ComponentMapper<SpatialComponent> sm;
  TagManager tagManager;

  @Wire
  UserManager userManager;
  @Wire
  ArenaServer arenaServer;

  Map<Connection, ClientInfo> clients = new ConcurrentHashMap<Connection, ClientInfo>();
  List<Component> events = new CopyOnWriteArrayList<Component>();

  @Override
  protected abstract void initialize();

  @Override
  protected void processSystem() {
    for (Component component : getEvents()) {
      Entity entity = world.createEntity();
      EntityEdit entityEdit = entity.edit();
      entityEdit.add(component);
    }
  }

  protected void addServerEvent(Component component) {
    events.add(component);
  }

  private List<Component> getEvents() {
    List<Component> c = new ArrayList<Component>();
    c.addAll(events);
    events.removeAll(c);
    return c;
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class ConnectionTerminatedListener implements ConnectionTerminatedPacket.Handler {

    @Override
    public void onConnectionTerminated(ConnectionTerminatedPacket packet, Connection connection) {
      Gdx.app.log("INFO", "Server: client connection terminated");
      ClientInfo clientInfo = clients.get(connection);
      if (clientInfo == null) {
        Gdx.app.log("INFO", "Server: connection without client terminated");
      } else {
        UserId userId = clientInfo.getUserId();
        if (userId != null) {
          Gdx.app.log("INFO", "Server: entity " + userId + " signed to be removed");
          LogoutPacket logoutPacket = new LogoutPacket(userId);
          LogoutServerEvent serverEvent = new LogoutServerEvent(connection, logoutPacket);
          Component component = new LogoutEventComponent(serverEvent);
          addServerEvent(component);
        }
        clients.remove(connection);
      }
      Gdx.app.log("INFO", "Server: connection removed");
    }

  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   *
   */
  class LoginListener implements LoginPacket.Handler {

    @Override
    public void onLogin(LoginPacket packet, Connection connection) {
      try {
        Gdx.app.log("INFO", "Server: client requests login");
        UserId userId = userManager.authenticate(packet.getUserName(), packet.getPassword());
        Gdx.app.log("INFO", "Server: client " + userId + " authenticated");
        if (connection.isConnected()) {
          ClientInfo clientInfo = new ClientInfo(userId);
          clients.put(connection, clientInfo);
          LoginSucceededPacket loginSucceededPacket =
              new LoginSucceededPacket(userId.getUsername());
          LoginSucceededServerEvent serverEvent =
              new LoginSucceededServerEvent(connection, loginSucceededPacket);
          Component component = new LoginEventComponent(serverEvent);
          addServerEvent(component);
        }
      } catch (UserIdNotFoundException e) {
        OperationFailedPacket response = new OperationFailedPacket("UserIdNotFoundException");
        arenaServer.send(Channel.TCP, connection, response);
      } catch (AuthenticationException e) {
        OperationFailedPacket response = new OperationFailedPacket("AuthenticationException");
        arenaServer.send(Channel.TCP, connection, response);
      }
    }

  }

}
