package it.rome.arena.server.system;

import it.rome.arena.server.component.RequestArenaQueueEventComponent;
import it.rome.arena.server.component.RequestUserInfoEventComponent;
import it.rome.arena.server.event.RequestArenaQueueServerEvent;
import it.rome.arena.server.event.RequestUserInfoServerEvent;
import it.rome.arena.shared.listener.packet.LoginPacket;
import it.rome.arena.shared.listener.packet.RequestArenaQueuePacket;
import it.rome.arena.shared.listener.packet.RequestUserInfoPacket;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.UserId;
import it.rome.game.shared.listener.packet.ConnectionTerminatedPacket;

import com.artemis.annotations.Wire;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire(injectInherited = true)
public class RealmNetworkingSystem extends ServerNetworkingSystem {

  @Override
  protected void initialize() {
    arenaServer.addHandler(LoginPacket.TYPE, new LoginListener());
    arenaServer.addHandler(ConnectionTerminatedPacket.TYPE, new ConnectionTerminatedListener());
    arenaServer.addHandler(RequestUserInfoPacket.TYPE, new RequestUserInfoListener());
    arenaServer.addHandler(RequestArenaQueuePacket.TYPE, new RequestArenaQueueListener());
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class RequestUserInfoListener implements RequestUserInfoPacket.Handler {

    @Override
    public void onUserInfoRequested(RequestUserInfoPacket packet, Connection connection) {
      ClientInfo clientInfo = clients.get(connection);
      RequestUserInfoServerEvent serverEvent =
          new RequestUserInfoServerEvent(clientInfo.getUserId(), connection, packet);
      RequestUserInfoEventComponent component = new RequestUserInfoEventComponent(serverEvent);
      addServerEvent(component);
    }

  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class RequestArenaQueueListener implements RequestArenaQueuePacket.Handler {

    @Override
    public void onArenaQueueRequested(RequestArenaQueuePacket packet, Connection connection) {
      ClientInfo clientInfo = clients.get(connection);
      UserId userId = clientInfo.getUserId();
      RequestArenaQueueServerEvent serverEvent =
          new RequestArenaQueueServerEvent(userId, connection, packet);
      RequestArenaQueueEventComponent component = new RequestArenaQueueEventComponent(serverEvent);
      addServerEvent(component);
    }

  }

}
