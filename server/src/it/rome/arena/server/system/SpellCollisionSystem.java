package it.rome.arena.server.system;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.component.SpellComponent;
import it.rome.game.attribute.EntityRef;
import it.rome.game.component.CollisionComponent;

import java.util.Collection;
import java.util.Collections;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class SpellCollisionSystem extends EntityProcessingSystem {

  ComponentMapper<SpellComponent> sm;
  ComponentMapper<CollisionComponent> cm;

  CreationsSystem creationsSystem;

  @SuppressWarnings("unchecked")
  public SpellCollisionSystem() {
    super(Aspect.getAspectForAll(SpellComponent.class, CollisionComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    CollisionComponent collisionComponent = cm.get(entity);
    Collection<EntityRef> contacts = collisionComponent.getContact();
    if (contacts != null && !contacts.isEmpty()) {
      SpellComponent spellComponent = sm.get(entity);
      SpellDamage spellDamage = spellComponent.getSpellDamage();
      for (EntityRef targetRef : contacts) {
        Entity target = targetRef.getEntity();
        if (target != null && spellDamage.canHit(target)) {
          createDamage(entity, contacts, spellDamage);
        }
      }
    }
  }

  private void createDamage(Entity entity, Collection<EntityRef> contacts, SpellDamage spellDamage) {
    Collection<EntityRef> targets = Collections.unmodifiableCollection(contacts);
    for (EntityRef target : targets) {
      creationsSystem.createDamage(spellDamage, target);
      entity.deleteFromWorld();
    }
  }

}
