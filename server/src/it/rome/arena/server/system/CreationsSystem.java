package it.rome.arena.server.system;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.server.persistence.SpellStore;
import it.rome.arena.server.template.DamageTemplate;
import it.rome.arena.server.template.PlayerTemplate;
import it.rome.arena.server.template.ServerMessageTemplate;
import it.rome.arena.server.template.SpellTemplate;
import it.rome.arena.shared.Group;
import it.rome.arena.shared.listener.packet.ServerMessage;
import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.SpatialComponent;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.template.ArenaTemplateGroups;
import it.rome.game.shared.template.TagTemplate;
import it.rome.game.system.HeadlessMapRenderSystem;
import it.rome.game.template.EntityFactory;
import it.rome.game.template.Parameters;
import it.rome.game.utils.CollectionsUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class CreationsSystem extends HeadlessMapRenderSystem {

  ComponentMapper<SpatialComponent> sm;

  GroupManager groupManager;

  @Wire
  SpellStore spellStore;

  EntityFactory entityFactory;
  BodyBuilder bodyBuilder;
  TagTemplate tagTemplate;
  SpellTemplate spellTemplate;
  PlayerTemplate playerTemplate;
  DamageTemplate damageTemplate;
  ServerMessageTemplate messageTemplate;

  final Queue<Spatial> checkpoints = CollectionsUtil.emptyQueue();
  final Map<Short, Spatial> assigned = CollectionsUtil.emptyMap();

  public CreationsSystem(String arenaName, World physicWorld) {
    super(arenaName, new ArenaTemplateGroups(physicWorld));
    this.bodyBuilder = new BodyBuilder(physicWorld);
    this.playerTemplate = new PlayerTemplate(bodyBuilder);
    this.spellTemplate = new SpellTemplate(bodyBuilder);
    this.tagTemplate = new TagTemplate();
    this.damageTemplate = new DamageTemplate();
    this.messageTemplate = new ServerMessageTemplate();
    addListener(listener);
  }

  @Override
  protected void initialize() {
    super.initialize();
    this.entityFactory = new EntityFactory(world);
  }

  public UUID createPlayer(ClientInfo clientInfo, Connection connection, String networkId,
      Stats stats) {
    Spatial spatial = getStartPoint(stats.getFaction());
    Collection<Spell> spellBook;
    try {
      spellBook = spellStore.getSpells(stats.getClazz());
    } catch (PersistenceException e) {
      Gdx.app.log("ERROR", "Server: error reading spellbook", e);
      spellBook = CollectionsUtil.toCollection(new ArrayList<Spell>());
    }
    UUID uuid =
        entityFactory.instantiate(
            new Parameters().put("clientInfo", clientInfo).put("spellBook", spellBook)
                .put("spatial", new SpatialImpl(spatial.getX(), spatial.getY(), 30f, 30f))
                .put("connection", connection).put("networkId", networkId).put("stats", stats)
                .put("tag", networkId), playerTemplate, tagTemplate).getUuid();
    Gdx.app.log("DEBUG", "Server: created player TAG: " + networkId);
    return uuid;
  }

  public void createSpell(Entity caster, Spell spell, int commandId, float x0, float y0, float x1,
      float y1) {
    UUID uuid =
        entityFactory.instantiate(
            new Parameters().put("spell", spell).put("x0", x0).put("y0", y0).put("x1", x1)
                .put("y1", y1).put("commandId", commandId).put("casterRef", new EntityRef(caster)),
            spellTemplate).getUuid();
    Gdx.app.log("DEBUG", "Server: created spell " + uuid.toString());
  }

  public void createDamage(SpellDamage spellDamage, EntityRef targets) {
    entityFactory.instantiate(
        new Parameters().put("spellDamage", spellDamage).put("targets", targets), damageTemplate);
  }

  public <SM extends ServerMessage<?>> void createMessage(SM message) {
    entityFactory.instantiate(new Parameters().put("message", message), messageTemplate);
    Gdx.app.log("DEBUG", "Server: created server message " + message);
  }

  @Override
  protected void processSystem() {}

  private Spatial getStartPoint(short faction) {
    Spatial position = assigned.get(faction);
    if (position == null) {
      position = checkpoints.poll();
      assigned.put(faction, position);
    }
    return position;
  }

  private final CreationListener listener = new CreationListener() {
    @Override
    public void onCreated(Entity entity) {
      ImmutableBag<String> groups = groupManager.getGroups(entity);
      for (String g : groups) {
        if (Group.CHECKPOINTS.equals(g)) {
          SpatialComponent spatialComponent = sm.get(entity);
          checkpoints.add(spatialComponent.getSpatial());
        }
      }
    }

  };

}
