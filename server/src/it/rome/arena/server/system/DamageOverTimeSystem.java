package it.rome.arena.server.system;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.component.DamageOverTimeComponent;
import it.rome.arena.shared.listener.packet.DamageServerMessage;
import it.rome.game.attribute.EntityRef;
import it.rome.game.component.TagComponent;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
@Wire
public class DamageOverTimeSystem extends EntityProcessingSystem {

  ComponentMapper<DamageOverTimeComponent> dotm;
  ComponentMapper<TagComponent> tm;

  CreationsSystem creationsSystem;

  @SuppressWarnings("unchecked")
  public DamageOverTimeSystem() {
    super(Aspect.getAspectForAll(DamageOverTimeComponent.class));
  }

  @Override
  protected void process(Entity entity) {
    DamageOverTimeComponent component = dotm.get(entity);
    SpellDamage dot = component.getSpellDamage();
    EntityRef ref = component.getTarget();
    if (ref != null) {
      Entity targetEnt = ref.getEntity();
      inflictDamage(component, dot, targetEnt);
      if (component.isFinished()) {
        entity.deleteFromWorld();
      }
    }
  }

  private void inflictDamage(DamageOverTimeComponent component, SpellDamage dot, Entity targetEnt) {
    if (targetEnt != null && tm.has(targetEnt)) {
      TagComponent tagComponent = tm.get(targetEnt);
      String target = tagComponent.getTag();
      component.update(world.getDelta());
      if (component.getExecute() > 0) {
        if (dot.canHit(targetEnt)) {
          int damage = dot.damage(targetEnt);
          DamageServerMessage message = new DamageServerMessage(target, damage);
          creationsSystem.createMessage(message);
          component.decrementExecute(1);
        }
      }
    }
  }

}
