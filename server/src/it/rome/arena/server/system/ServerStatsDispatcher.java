package it.rome.arena.server.system;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.packet.utils.ServerPacketUtils;
import it.rome.arena.shared.listener.packet.StatsWorldPacket;
import it.rome.game.shared.Channel;
import it.rome.game.shared.component.StatsComponent;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.IntervalEntitySystem;
import com.artemis.utils.ImmutableBag;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
@Wire
public class ServerStatsDispatcher extends IntervalEntitySystem {

  @Wire
  ArenaServer arenaServer;

  @SuppressWarnings("unchecked")
  public ServerStatsDispatcher(float interval) {
    super(Aspect.getAspectForAll(StatsComponent.class), interval / 1000);
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {
    StatsWorldPacket packet = ServerPacketUtils.createStatsSnapshot(entities);
    arenaServer.send(Channel.TCP, packet);
  }

}
