package it.rome.arena.server.system;

import it.rome.arena.server.ArenaServer;
import it.rome.arena.server.Fight;
import it.rome.arena.shared.GameState;
import it.rome.arena.shared.Group;
import it.rome.arena.shared.listener.packet.GameStatePacket;
import it.rome.arena.shared.listener.packet.PlayerDeadPacket;
import it.rome.game.attribute.Health;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.PlayerComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.fsm.FSM;
import it.rome.game.shared.Channel;
import it.rome.game.shared.component.DeadComponent;
import it.rome.game.shared.component.FactionComponent;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.shared.component.WeaponComponent;
import it.rome.game.utils.CollectionsUtil;

import java.util.Collection;
import java.util.Map;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.EntitySystem;
import com.artemis.annotations.Wire;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.utils.ImmutableBag;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
@Wire
public class DirectorSystem extends EntitySystem {

  ComponentMapper<StatsComponent> stm;
  ComponentMapper<HealthComponent> hm;
  ComponentMapper<DeadComponent> dm;
  ComponentMapper<FactionComponent> fm;
  ComponentMapper<TagComponent> tm;

  GroupManager groupManager;
  TagManager tagManager;

  CreationsSystem creationsSystem;

  @Wire
  ArenaServer arenaServer;
  @Wire
  ArenaGame arenaGame;

  private GameStatePacket gameStatePacket;

  public DirectorSystem() {
    this(Fight.DEBUG);
  }

  @SuppressWarnings("unchecked")
  public DirectorSystem(Fight fight) {
    super(Aspect.getAspectForAll(PlayerComponent.class, StatsComponent.class, TagComponent.class,
        HealthComponent.class).exclude(DeadComponent.class));
    this.arenaGame = new ArenaGame(fight);
  }

  @Override
  protected void initialize() {
    this.gameStatePacket = new GameStatePacket(this.arenaGame.getState());
  }

  @Override
  protected void inserted(Entity entity) {
    TagComponent tagComponent = tm.get(entity);
    arenaGame.addPlayer(tagComponent.getTag());
  }

  @Override
  protected void removed(Entity entity) {
    TagComponent tagComponent = tm.get(entity);
    arenaGame.removePlayer(tagComponent.getTag());
  }

  @Override
  protected void processEntities(ImmutableBag<Entity> entities) {
    for (Entity entity : entities) {
      HealthComponent healthComponent = hm.get(entity);
      Health health = healthComponent.getHealth();
      if (health.getLife() <= 0) {
        TagComponent tagComponent = tm.get(entity);
        PlayerDeadPacket message = new PlayerDeadPacket(tagComponent.getTag());
        creationsSystem.createMessage(message);
        killPlayer(entity);
      }
    }
    arenaGame.update(world.getDelta());
  }

  private void notifyGameState() {
    gameStatePacket.setGameState(arenaGame.getState());
    arenaServer.send(Channel.TCP, gameStatePacket);
  }

  private Short getFaction(String player) {
    Entity e = tagManager.getEntity(player);
    if (e != null) {
      return fm.get(e).getFaction();
    }
    return null;
  }

  private boolean isAlive(String player) {
    Entity e = tagManager.getEntity(player);
    if (e != null) {
      if (dm.has(e)) {
        return false;
      }
      HealthComponent healthComponent = hm.get(e);
      Health health = healthComponent.getHealth();
      int life = health.getLife();
      return life > 0;
    }
    return false;
  }

  private void killPlayer(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    entityEdit.remove(HealthComponent.class);
    entityEdit.remove(WeaponComponent.class);
    entityEdit.add(new DeadComponent());
  }

  /**
   * 
   * @author marco
   * @since 1.0.0
   *
   */
  class ArenaGame extends FSM<GameState> {

    private final Fight fight;
    private final Map<Short, Collection<String>> parties = CollectionsUtil.emptyMap();
    private Short winner = null;

    float delta = 0;

    public ArenaGame(Fight fight) {
      this.fight = fight;
      createState(GameState.INITIALIZING, new GameInitialize());
      createState(GameState.READY, new GameReady());
      createState(GameState.FIGHTING, new GameFight());
      createState(GameState.ENDED, new GameEnd());
      setState(GameState.INITIALIZING);
    }

    public void addPlayer(String player) {
      Short faction = getFaction(player);
      Collection<String> party = parties.get(faction);
      if (party == null) {
        party = CollectionsUtil.emptySet();
        parties.put(faction, party);
      }
      party.add(player);
    }

    public void removePlayer(String player) {
      Short faction = getFaction(player);
      Collection<String> party = parties.get(faction);
      if (party != null) {
        party.remove(player);
      }
    }

    public void update(float delta) {
      this.delta = delta;
      super.update();
    }

    public Short getWinner() {
      return winner;
    }

    /**
     * 
     * @author marco
     * @since 1.0.0
     *
     */
    class GameInitialize extends State {

      @Override
      public void start() {
        notifyGameState();
      }

      @Override
      public void update() {
        int challengers = 0;
        for (Short faction : parties.keySet()) {
          Collection<String> party = parties.get(faction);
          challengers += party.size();
        }
        if (challengers == fight.getPlayers()) {
          changeState(GameState.READY);
        }
      }

    }


    /**
     * 
     * @author marco
     * @since 1.0.0
     *
     */
    class GameReady extends State {

      float readySet = 0;

      @Override
      public void start() {
        notifyGameState();
      }

      @Override
      public void update() {
        readySet += delta;
        if (readySet >= 10) {
          changeState(GameState.FIGHTING);
        }
      }

    }


    /**
     * 
     * @author marco
     * @since 1.0.0
     *
     */
    class GameFight extends State {

      @Override
      public void start() {
        for (Entity entity : groupManager.getEntities(Group.GATES)) {
          entity.deleteFromWorld();
        }
        notifyGameState();
      }

      @Override
      public void update() {
        int alive = 0;
        for (Short faction : parties.keySet()) {
          Collection<String> party = parties.get(faction);
          for (String player : party) {
            if (isAlive(player)) {
              alive++;
              winner = faction;
              break;
            }
          }
        }
        if (alive == 1) {
          changeState(GameState.ENDED);
        }
      }

    }

    /**
     * 
     * @author marco
     * @since 1.0.0
     *
     */
    class GameEnd extends State {

      @Override
      public void start() {
        notifyGameState();
      }

    }

  }

}
