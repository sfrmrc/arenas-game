package it.rome.arena.server.template;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.component.SpellComponent;
import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.ScriptJava;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.CasterComponent;
import it.rome.game.component.CollisionComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.ScriptComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.impl.SpellShared;
import it.rome.game.shared.component.FactionComponent;
import it.rome.game.shared.component.SharedComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.World;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author marco
 * @since 0.0.1
 * @since 1.0.0
 *
 */
public class SpellTemplate extends EntityTemplate {

  private final BodyBuilder bodyBuilder;

  public SpellTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    Spell spell = parameters.get("spell");
    EntityRef casterRef = parameters.get("casterRef");
    float x0 = parameters.get("x0");
    float y0 = parameters.get("y0");
    float x1 = parameters.get("x1", x0);
    float y1 = parameters.get("y1", y0);
    float speed = parameters.get("speed", 500f);
    int commandId = parameters.get("commandId", -1);

    if (speed == 0) {
      throw new IllegalArgumentException("Speed parameter cannot be null");
    }

    if (commandId == -1) {
      throw new IllegalArgumentException("CommandId parameter cannot be null");
    }

    float radius = 10f;

    FactionComponent factionComponent =
        casterRef.getEntity() != null
            ? casterRef.getEntity().getComponent(FactionComponent.class)
            : null;

    if (factionComponent == null) {
      throw new IllegalArgumentException("Caster entity must have FactionComponent");
    }

    TagComponent tagComponent = casterRef.getEntity().getComponent(TagComponent.class);

    if (tagComponent == null) {
      throw new IllegalArgumentException("Caster entity must have TagComponent");
    }

    short faction = factionComponent.getFaction();
    short mask = (short) ~faction;

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(x0, y0)
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(radius * 0.3f).restitution(0f)
                    .density(0f).friction(0f).categoryBits(faction).maskBits(mask)).build();

    Spatial spatial = new SpatialImpl(x0, y0, radius, radius);
    spatial.setAngle(getAngle(x0, y0, x1, y1));
    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    SpatialComponent spatialComponent = new SpatialComponent(new SpatialPhysicsImpl(body, spatial));

    Vector2 s = new Vector2(x1 - x0, y1 - y0);
    s.nor().scl(speed);

    Shared shared =
        new SpellShared(entity.getUuid().toString(), tagComponent.getTag(), commandId, spatial, s);

    SpellDamage spellDamage = new SpellDamage(tagComponent.getTag(), spell);

    entityEdit.add(new SpellComponent(spellDamage));
    entityEdit.add(new SharedComponent(shared));
    entityEdit.add(new CasterComponent(casterRef));
    entityEdit.add(new VelocityComponent(s.x, s.y));
    entityEdit.add(spatialComponent);
    entityEdit.add(physicsComponent);
    entityEdit.add(new CollisionComponent());
    entityEdit.add(new ScriptComponent(new SpellScript(spell, spatial)));

  }

  public static float getAngle(float x0, float y0, float x1, float y1) {
    double xDiff = x1 - x0;
    double yDiff = y1 - y0;
    return new Double(Math.toDegrees(Math.atan2(yDiff, xDiff))).floatValue();
  }

  /**
   * 
   * @author marco
   * @since 0.0.1
   * @since 1.0.0
   *
   */
  private class SpellScript extends ScriptJava {

    private final int range;
    private final Spatial startPoint;

    public SpellScript(Spell spell, Spatial startPoint) {
      this.startPoint = startPoint;
      this.range = spell.getRange();
    }

    @Override
    public void execute(World world, Entity entity) {
      Spatial spatial = entity.getComponent(SpatialComponent.class).getSpatial();
      int contacts = entity.getComponent(CollisionComponent.class).getContact().size();
      float distance =
          Vector2.dst(startPoint.getX(), startPoint.getY(), spatial.getX(), spatial.getY());
      if (distance >= range || contacts != 0) {
        finished = true;
        entity.deleteFromWorld();
        return;
      }

    }

  }

}
