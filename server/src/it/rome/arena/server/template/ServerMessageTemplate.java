package it.rome.arena.server.template;

import it.rome.arena.server.component.ServerMessageComponent;
import it.rome.arena.shared.listener.packet.ServerMessage;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ServerMessageTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    ServerMessage<?> message = parameters.get("message");
    if (message == null) {
      throw new IllegalArgumentException("Message parameter cannot be null");
    }
    entityEdit.add(new ServerMessageComponent(message));
  }

}
