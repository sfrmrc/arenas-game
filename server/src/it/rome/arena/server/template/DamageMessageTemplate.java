package it.rome.arena.server.template;

import it.rome.arena.server.component.ServerMessageComponent;
import it.rome.arena.shared.listener.packet.DamageServerMessage;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class DamageMessageTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String target = parameters.get("target");
    Integer damage = parameters.get("damage", 0);
    DamageServerMessage message = new DamageServerMessage(target, damage);
    entityEdit.add(new ServerMessageComponent(message));
  }
  
}
