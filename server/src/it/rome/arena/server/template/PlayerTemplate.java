package it.rome.arena.server.template;

import it.rome.arena.server.component.NetworkComponent;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.PlayerComponent;
import it.rome.game.component.PointsComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.ClientInfo;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.attribute.impl.PlayerShared;
import it.rome.game.shared.component.FactionComponent;
import it.rome.game.shared.component.SharedComponent;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.shared.component.WeaponComponent;
import it.rome.game.template.EntityTemplate;

import java.util.Collection;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.esotericsoftware.kryonet.Connection;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class PlayerTemplate extends EntityTemplate {

  final BodyBuilder bodyBuilder;

  public PlayerTemplate(BodyBuilder bodyBuilder) {
    super();
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    ClientInfo clientInfo = parameters.get("clientInfo");
    String networkId = parameters.get("networkId");
    Connection connection = parameters.get("connection");
    Spatial initialSpatial = parameters.get("spatial");
    Stats stats = parameters.get("stats");
    Collection<Spell> spellBook = parameters.get("spellBook");

    if (clientInfo == null) {
      throw new IllegalArgumentException("ClientInfo cannot be null");
    }

    if (networkId == null) {
      throw new IllegalArgumentException("NetworkId cannot be null");
    }

    if (connection == null) {
      throw new IllegalArgumentException("Connection cannot be null");
    }

    if (initialSpatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    if (initialSpatial.getWidth() <= 0 || initialSpatial.getHeight() <= 0) {
      throw new IllegalArgumentException("Illegal spatial parameter");
    }

    if (stats == null) {
      throw new IllegalArgumentException("Stats parameter cannot be null");
    }

    short faction = stats.getFaction();
    short mask = (short) ~faction;

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(initialSpatial.getX(), initialSpatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(initialSpatial.getWidth() * 0.5f)
                    .maskBits(mask).categoryBits(faction).restitution(0f).friction(0f).density(1f)
                    .group((short) -1)).build();

    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    SpatialPhysicsImpl spatial = new SpatialPhysicsImpl(body, initialSpatial);
    SpatialComponent spatialComponent = new SpatialComponent(spatial);
    VelocityComponent velocityComponent = new VelocityComponent(0);
    PlayerShared shared =
        new PlayerShared(networkId, stats, new SpatialImpl(spatial), velocityComponent.getSpeed());

    entityEdit.add(new PlayerComponent());
    entityEdit.add(new SharedComponent(shared));
    entityEdit.add(new NetworkComponent(clientInfo, connection));
    entityEdit.add(velocityComponent);
    entityEdit.add(spatialComponent);
    entityEdit.add(physicsComponent);
    entityEdit.add(new HealthComponent(stats.getHealth()));
    entityEdit.add(new PointsComponent(stats.getPoints()));
    entityEdit.add(new StatsComponent(stats));
    entityEdit.add(new FactionComponent(stats.getFaction()));
    entityEdit.add(new WeaponComponent(spellBook));

  }

}
