package it.rome.arena.server.template;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.server.component.DamageOverTimeComponent;
import it.rome.arena.server.component.DirectDamageComponent;
import it.rome.arena.shared.SpellType;
import it.rome.game.attribute.EntityRef;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class DamageTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    SpellDamage spellDamage = parameters.get("spellDamage");
    EntityRef target = parameters.get("targets");

    Spell spell = spellDamage.getSpell();
    SpellType spellType = spell.getType();
    switch (spellType) {
      case DIRECT:
        entityEdit.add(new DirectDamageComponent(spellDamage, target));
        break;
      case DOT:
        entityEdit.add(new DamageOverTimeComponent(spellDamage, target));
        break;
      case AREA:
      case HOT:
        break;
    }

  }
}
