package it.rome.arena.server.template;

import it.rome.arena.server.component.ServerMessageComponent;
import it.rome.arena.shared.listener.packet.PlayerDeadPacket;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class DeadMessageTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String networkId = parameters.get("networkId");
    PlayerDeadPacket message = new PlayerDeadPacket(networkId);
    entityEdit.add(new ServerMessageComponent(message));
  }

}
