package it.rome.arena.server;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public enum Fight {

  DEBUG(2), Pvp2(4), Pvp3(6), Pvp5(10);

  final int players;

  private Fight(int players) {
    this.players = players;
  }

  public int getPlayers() {
    return players;
  }

}
