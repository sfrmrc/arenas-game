package it.rome.arena.server;

import it.rome.arena.shared.config.ArenaConfig;
import it.rome.game.shared.Channel;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.Event.Type;
import it.rome.game.shared.listener.EventBus;
import it.rome.game.shared.listener.HandlerRegistration;
import it.rome.game.shared.listener.NetworkListener;
import it.rome.game.shared.listener.packet.Packet;

import java.io.IOException;
import java.net.ConnectException;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class ArenaServer {

  int tcpPort, udpPort;
  Server server;
  NetworkListener serverListener;
  EventBus eventBus;

  public ArenaServer() {
    this(ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, ArenaConfig.packets,
        new EventBus());
  }

  public ArenaServer(EventBus eventBus) {
    this(ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, ArenaConfig.packets, eventBus);
  }

  public ArenaServer(Packet packet, EventBus eventBus) {
    this(ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, packet, eventBus);
  }

  public ArenaServer(int tcpPort, int udpPort, Packet packet, EventBus eventBus) {
    this.tcpPort = tcpPort;
    this.udpPort = udpPort;
    this.eventBus = eventBus;
    this.server = new Server();
    packet.register(server);
    initListener(eventBus);
  }

  public <H> HandlerRegistration addHandler(Type<H> type, H handler) {
    return eventBus.addHandler(type, handler);
  }

  private void initListener(EventBus eventBus) {
    this.serverListener = new NetworkListener(eventBus);
    this.server.addListener(serverListener);
  }

  public void start() throws ConnectException {
    try {
      this.server.bind(tcpPort, udpPort);
      this.server.start();
    } catch (IOException e) {
      throw new ConnectException(e.getMessage());
    }
  }

  public <P extends BasePacket<?>> void send(Channel channel, P packet) {
    if (channel == Channel.TCP) {
      server.sendToAllTCP(packet);
    }
    if (channel == Channel.UDP) {
      server.sendToAllUDP(packet);
    }
  }

  public <P extends BasePacket<?>> void send(Channel channel, Connection connection, P packet) {
    if (channel == Channel.TCP) {
      connection.sendTCP(packet);
    }
    if (channel == Channel.UDP) {
      connection.sendUDP(packet);
    }
  }

  public void stop() {
    server.close();
  }

  public int getTcpPort() {
    return tcpPort;
  }

  public int getUdpPort() {
    return udpPort;
  }

}
