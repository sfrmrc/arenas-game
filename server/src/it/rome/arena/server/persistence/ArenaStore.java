package it.rome.arena.server.persistence;

import it.rome.arena.server.exception.PersistenceException;
import it.rome.game.shared.Arena;

import java.util.Collection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public interface ArenaStore {

  public Arena getArena(String name) throws PersistenceException;

  public Collection<Arena> getArena() throws PersistenceException;

  public void pubArena(Arena arena) throws PersistenceException;

  public void removeArena(Arena arena) throws PersistenceException;

}
