package it.rome.arena.server.persistence;

import it.rome.arena.server.exception.PersistenceException;
import it.rome.game.shared.UserId;
import it.rome.game.shared.attribute.Stats;

import java.util.Collection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public interface CharacterStore {

  public Stats getStats(String character) throws PersistenceException;

  public void putStats(UserId userId, Stats stats) throws PersistenceException;

  public void removeStats(Stats stats) throws PersistenceException;

  public Collection<Stats> getStatsByUser(UserId userId) throws PersistenceException;

}
