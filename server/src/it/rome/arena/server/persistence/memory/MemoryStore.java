package it.rome.arena.server.persistence.memory;

import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.server.persistence.CharacterStore;
import it.rome.arena.server.persistence.SpellStore;
import it.rome.arena.server.persistence.UserStore;
import it.rome.arena.shared.Class;
import it.rome.arena.shared.Race;
import it.rome.arena.shared.SpellCast;
import it.rome.arena.shared.SpellType;
import it.rome.game.shared.UserId;
import it.rome.game.shared.attribute.Attributes.TYPE;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.utils.CollectionsUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class MemoryStore implements UserStore, CharacterStore, SpellStore {

  private static final short FACTION_A = 0x0001;
  private static final short FACTION_B = 0x0002;

  private final Map<String, UserId> userStore = new ConcurrentHashMap<String, UserId>();
  private final Map<String, Stats> statStore = new ConcurrentHashMap<String, Stats>();
  private final Map<String, Collection<Stats>> userStatStore =
      new ConcurrentHashMap<String, Collection<Stats>>();
  private final Map<Integer, Spell> spellStore = new ConcurrentHashMap<Integer, Spell>();
  private final Map<Class, Collection<Spell>> classSpellStore =
      new ConcurrentHashMap<Class, Collection<Spell>>();

  public MemoryStore() {
    initUsers();
    initCharacters();
    initSpells();
  }

  private void initUsers() {
    userStore.put("user1", new UserId("user1"));
    userStore.put("user2", new UserId("user2"));
  }

  private void initCharacters() {
    Stats.Builder builder = new Stats.Builder();
    Stats stats1 =
        builder.name("user1").faction(FACTION_A).race(Race.HUMAN).type(Class.ARCHER).build();
    Stats stats2 = builder.name("user2").faction(FACTION_B).race(Race.ELF).type(Class.MAGE).build();
    statStore.put("user1", stats1);
    statStore.put("user2", stats2);
    userStatStore.put("user1", CollectionsUtil.toList(stats1));
    userStatStore.put("user2", CollectionsUtil.toList(stats2));
  }

  private void initSpells() {
    Spell.Builder builder = new Spell.Builder();
    /* MAGE */
    Spell spell0 = builder.id(62).damage(0).range(100).type(SpellType.DIRECT).build();
    Spell spell1 =
        builder.id(61).damage(1).range(200).attr(TYPE.DOT, 2).attr(TYPE.DRT, 10)
            .type(SpellType.DOT).cast(SpellCast.CD).build();
    /* ARCHER */
    Spell spell2 = builder.id(72).damage(0).range(100).type(SpellType.DIRECT).build();
    Spell spell3 = builder.id(67).damage(2).range(200).type(SpellType.DIRECT).build();

    addSpell2Class(Class.MAGE, spell0, spell1);
    addSpell2Class(Class.ARCHER, spell2, spell3);
  }

  private void addSpell2Class(Class clazz, Spell... spells) {
    Collection<Spell> spellBook = new ArrayList<Spell>();
    for (Spell s : spells) {
      spellStore.put(s.getId(), s);
      spellBook.add(s);
    }
    classSpellStore.put(clazz, spellBook);
  }

  @Override
  public UserId getUserId(String username) throws PersistenceException {
    return userStore.get(username);
  }

  @Override
  public void putUserId(UserId userId) throws PersistenceException {
    userStore.put(userId.getUsername(), userId);
    userStatStore.put(userId.getUsername(), new ArrayList<Stats>());
  }

  @Override
  public void removeUserId(UserId userId) throws PersistenceException {
    userStore.remove(userId.getUsername());
    userStatStore.remove(userId.getUsername());
  }

  @Override
  public Stats getStats(String character) throws PersistenceException {
    return statStore.get(character);
  }

  @Override
  public void putStats(UserId userId, Stats stats) throws PersistenceException {
    statStore.put(stats.getName(), stats);
    Collection<Stats> l = userStatStore.get(userId.getUsername());
    l.add(stats);
  }

  @Override
  public void removeStats(Stats stats) throws PersistenceException {
    statStore.remove(stats.getName());
    for (Collection<Stats> l : userStatStore.values()) {
      if (l.contains(stats)) {
        l.remove(stats);
        return;
      }
    }
  }

  @Override
  public Collection<Stats> getStatsByUser(UserId userId) throws PersistenceException {
    return userStatStore.get(userId.getUsername());
  }

  @Override
  public Collection<Spell> getSpell() throws PersistenceException {
    try {
      return Collections.unmodifiableCollection(spellStore.values());
    } catch (Exception e) {
      throw new PersistenceException(e);
    }
  }

  @Override
  public Collection<Spell> getSpells(Class clazz) throws PersistenceException {
    Collection<Spell> spells = classSpellStore.get(clazz);
    if (spells == null) {
      return new ArrayList<Spell>();
    }
    return spells;
  }

  @Override
  public Spell getSpell(int id) throws PersistenceException {
    Spell spell = spellStore.get(id);
    if (spell == null) {
      throw new PersistenceException();
    }
    return spell;
  }

  @Override
  public void putSpell(Spell spell) throws PersistenceException {
    try {
      spellStore.put(spell.getId(), spell);
    } catch (Exception e) {
      throw new PersistenceException(e);
    }
  }

  @Override
  public void removeSpell(Spell spell) throws PersistenceException {
    try {
      spellStore.remove(spell.getId());
    } catch (Exception e) {
      throw new PersistenceException(e);
    }
  }

}
