package it.rome.arena.server.persistence;

import it.rome.arena.server.exception.PersistenceException;
import it.rome.game.shared.UserId;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public interface UserStore {

  public UserId getUserId(String username) throws PersistenceException;

  public void putUserId(UserId userId) throws PersistenceException;

  public void removeUserId(UserId userId) throws PersistenceException;

}
