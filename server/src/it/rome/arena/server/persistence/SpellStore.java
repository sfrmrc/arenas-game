package it.rome.arena.server.persistence;

import it.rome.arena.server.exception.PersistenceException;
import it.rome.arena.shared.Class;
import it.rome.game.shared.attribute.Spell;

import java.util.Collection;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public interface SpellStore {

  public Collection<Spell> getSpell() throws PersistenceException;

  public Spell getSpell(int id) throws PersistenceException;

  public Collection<Spell> getSpells(Class clazz) throws PersistenceException;

  public void putSpell(Spell spell) throws PersistenceException;

  public void removeSpell(Spell spell) throws PersistenceException;

}
