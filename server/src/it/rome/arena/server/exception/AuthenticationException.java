package it.rome.arena.server.exception;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class AuthenticationException extends Exception {

  private static final long serialVersionUID = -8791758848297290705L;

  public AuthenticationException() {
    super();
  }

  public AuthenticationException(String message) {
    super(message);
  }

  public AuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public AuthenticationException(Throwable cause) {
    super(cause);
  }

}
