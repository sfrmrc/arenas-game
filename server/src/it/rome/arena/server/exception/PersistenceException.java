package it.rome.arena.server.exception;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class PersistenceException extends Exception {

  private static final long serialVersionUID = -2531254766541037866L;

  public PersistenceException() {
    super();
  }

  public PersistenceException(String message) {
    super(message);
  }

  public PersistenceException(String message, Throwable cause) {
    super(message, cause);
  }

  public PersistenceException(Throwable cause) {
    super(cause);
  }

}
