package it.rome.arena.server.exception;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class CharacterNotFoundException extends Exception {

  private static final long serialVersionUID = 3534161349344603557L;

  public CharacterNotFoundException() {
    super();
  }

  public CharacterNotFoundException(String message) {
    super(message);
  }

  public CharacterNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public CharacterNotFoundException(Throwable cause) {
    super(cause);
  }

}
