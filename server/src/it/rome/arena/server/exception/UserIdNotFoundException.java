package it.rome.arena.server.exception;


/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class UserIdNotFoundException extends AuthenticationException {

  private static final long serialVersionUID = -514577712482970267L;

  public UserIdNotFoundException() {
    super();
  }

  public UserIdNotFoundException(String message) {
    super(message);
  }

  public UserIdNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public UserIdNotFoundException(Throwable cause) {
    super(cause);
  }

}
