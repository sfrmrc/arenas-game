package it.rome.arena.server.packet.utils;

import it.rome.arena.shared.listener.packet.AddEntityPacket;
import it.rome.arena.shared.listener.packet.CreatePlayerPacket;
import it.rome.arena.shared.listener.packet.EntityUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsUpdatePacket;
import it.rome.arena.shared.listener.packet.StatsWorldPacket;
import it.rome.arena.shared.listener.packet.WorldInfoPacket;
import it.rome.game.attribute.Spatial;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.TagComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.attribute.Shared;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.attribute.impl.MotionShared;
import it.rome.game.shared.component.SharedComponent;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.utils.CollectionsUtil;

import java.util.Collection;
import java.util.List;

import com.artemis.Entity;
import com.artemis.utils.ImmutableBag;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class ServerPacketUtils {

  private ServerPacketUtils() {}

  public static EntityUpdatePacket[] createWorldUpdate(ImmutableBag<Entity> entities) {
    List<EntityUpdatePacket> entitiesUpdate = CollectionsUtil.emptyList();
    for (Entity entity : entities) {
      entitiesUpdate.add(createEntityUpdatePacket(entity));
    }
    EntityUpdatePacket[] packets = new EntityUpdatePacket[entitiesUpdate.size()];
    return entitiesUpdate.toArray(packets);
  }

  public static WorldInfoPacket createWorldSnapshot(String arenaName, ImmutableBag<Entity> entities) {
    WorldInfoPacket worldInfoPacket = new WorldInfoPacket(arenaName);
    List<AddEntityPacket> addEntities = CollectionsUtil.emptyList();
    for (Entity entity : entities) {
      addEntities.add(createAddEntityPacket(entity));
    }
    AddEntityPacket[] packets = new AddEntityPacket[addEntities.size()];
    worldInfoPacket.setAddEnities(addEntities.toArray(packets));
    return worldInfoPacket;
  }

  public static AddEntityPacket createAddEntityPacket(Entity entity) {
    SharedComponent sharedComponent = entity.getComponent(SharedComponent.class);
    Shared shared = sharedComponent.getShared();
    AddEntityPacket addEntity = new AddEntityPacket(shared);
    return addEntity;
  }

  public static CreatePlayerPacket createNewPlayerPacket(Entity entity, Collection<Spell> spellBook) {
    SharedComponent sharedComponent = entity.getComponent(SharedComponent.class);
    CreatePlayerPacket createPlayerPacket = new CreatePlayerPacket(sharedComponent.getShared(), spellBook);
    return createPlayerPacket;
  }

  public static StatsWorldPacket createStatsSnapshot(ImmutableBag<Entity> entities) {
    StatsWorldPacket packet = new StatsWorldPacket();
    StatsUpdatePacket[] statsUpdatePackets = new StatsUpdatePacket[entities.size()];
    for (int i = 0; i < entities.size(); i++) {
      StatsUpdatePacket statsUpdatePacket = createStatsUpdatePacket(entities.get(i));
      statsUpdatePackets[i] = statsUpdatePacket;
    }
    packet.setStatsUpdatePackets(statsUpdatePackets);
    return packet;
  }

  private static StatsUpdatePacket createStatsUpdatePacket(Entity entity) {
    TagComponent tagComponent = entity.getComponent(TagComponent.class);
    StatsComponent statsComponent = entity.getComponent(StatsComponent.class);
    StatsUpdatePacket packet =
        new StatsUpdatePacket(tagComponent.getTag(), statsComponent.getStats());
    return packet;
  }

  // TODO: use shared data instead of spatial and velocity components
  private static EntityUpdatePacket createEntityUpdatePacket(Entity entity) {
    SharedComponent sharedComponent = entity.getComponent(SharedComponent.class);
    Shared shared = sharedComponent.getShared();
    if (shared instanceof MotionShared) {
      SpatialComponent spatialComponent = entity.getComponent(SpatialComponent.class);
      VelocityComponent velocityComponent = entity.getComponent(VelocityComponent.class);
      Spatial spatial = spatialComponent.getSpatial();
      EntityUpdatePacket entityUpdatePacket =
          new EntityUpdatePacket(shared.getNetworkId(), spatial.getX(), spatial.getY(),
              spatial.getAngle(), velocityComponent.getSpeed());
      return entityUpdatePacket;
    }
    return null;
  }

}
