package it.rome.arena.server.component;

import it.rome.arena.server.attribute.SpellDamage;
import it.rome.arena.shared.SpellType;
import it.rome.game.attribute.EntityRef;
import it.rome.game.shared.attribute.Attributes.TYPE;
import it.rome.game.shared.attribute.Spell;

import org.junit.Test;

/**
 * 
 * @author marco
 * @since 1.0.0
 *
 */
public class DamageOverTimeComponentTest {

  @Test
  public void testDamageOverTime() throws InterruptedException {
    EntityRef target = new EntityRef(null);
    Spell spell =
        new Spell.Builder().id(61).damage(1).range(200).attr(TYPE.DOT, 200).attr(TYPE.DRT, 1000)
            .type(SpellType.DOT).build();
    SpellDamage spellDamage = new SpellDamage("me", spell);
    DamageOverTimeComponent component = new DamageOverTimeComponent(spellDamage, target);
    long start = System.currentTimeMillis();
    int hits = 0;
    while (!component.isFinished()) {
      Thread.sleep(300);
      long delta = System.currentTimeMillis() - start;
      component.update(delta);
      if (component.getExecute() > 0) {
        component.decrementExecute(1);
        hits++;
      }
      start = System.currentTimeMillis();
    }
    System.out.println(hits);
  }

}
