package it.rome.arena.client.component;

import com.artemis.Component;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class LocalNetworkComponent extends Component {

  String networkId;
  int tick = 0;

  public LocalNetworkComponent(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public int getTick() {
    return tick;
  }

  public void incrementTick() {
    tick++;
  }

}
