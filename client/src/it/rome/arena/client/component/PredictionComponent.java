package it.rome.arena.client.component;

import it.rome.game.shared.attribute.Memento;
import it.rome.game.utils.CollectionsUtil;

import java.util.List;

import com.artemis.Component;
import com.badlogic.gdx.utils.Array;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 *
 */
public class PredictionComponent extends Component {

  private static final int MAX_MEMENTO = 100;

  Memento current;
  Array<Memento> positions = new Array<Memento>();

  public void setCurrent(Memento memento) {
    this.current = memento;
  }

  public Memento getCurrent() {
    return current;
  }

  public List<Memento> getMemento() {
    return CollectionsUtil.toList(positions);
  }

  public void add(Memento memento) {
    if (positions.size > MAX_MEMENTO) {
      positions.removeIndex(0);
    }
    positions.add(memento);
    for (int i = 0; i < positions.size; i++) {
      if (memento.getTick() <= current.getTick()) {
        positions.removeIndex(i);
      }
    }
  }

}
