package it.rome.arena.client;

import it.rome.arena.shared.Race;
import it.rome.game.utils.CollectionsUtil;

import java.util.EnumMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * 
 * @author marco
 * @since 0.0.1
 */
public class Images {

  private final static EnumMap<Race, Texture> races = new EnumMap<Race, Texture>(Race.class);

  private final static Map<Integer, Texture> spells = CollectionsUtil.emptyMap();

  static {

    races.put(Race.HUMAN, new Texture(Gdx.files.internal("human.png")));
    races.put(Race.ELF, new Texture(Gdx.files.internal("elf.png")));

    spells.put(62, new Texture(Gdx.files.internal("red_laser.png")));
    spells.put(61, new Texture(Gdx.files.internal("red_laser.png")));

    spells.put(72, new Texture(Gdx.files.internal("arrow.png")));
    spells.put(67, new Texture(Gdx.files.internal("arrow.png")));

  }

  public static final Texture race(Race race) {
    if (!races.containsKey(race)) {
      throw new RuntimeException("Race " + race + " not found as Texture");
    }
    return races.get(race);
  }

  public static final Texture spell(Integer spellId) {
    if (!spells.containsKey(spellId)) {
      throw new RuntimeException("Spell " + spellId + " not found as Texture");
    }
    return spells.get(spellId);
  }

  private Images() {}

}
