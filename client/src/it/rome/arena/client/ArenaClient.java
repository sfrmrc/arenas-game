package it.rome.arena.client;

import it.rome.arena.shared.config.ArenaConfig;
import it.rome.game.shared.Channel;
import it.rome.game.shared.listener.BasePacket;
import it.rome.game.shared.listener.Event.Type;
import it.rome.game.shared.listener.EventBus;
import it.rome.game.shared.listener.HandlerRegistration;
import it.rome.game.shared.listener.NetworkListener;
import it.rome.game.shared.listener.packet.Packet;

import java.io.IOException;
import java.net.ConnectException;

import com.esotericsoftware.kryonet.Client;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class ArenaClient {

  Client client;

  String host;
  int tcpPort, udpPort;
  EventBus eventBus;
  NetworkListener clientListener;

  public ArenaClient(String host) {
    this(host, ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, ArenaConfig.packets,
        new EventBus());
  }

  public ArenaClient(String host, EventBus eventBus) {
    this(host, ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, ArenaConfig.packets,
        eventBus);
  }

  public ArenaClient(String host, Packet packet, EventBus eventBus) {
    this(host, ArenaConfig.DEFAULT_TCP_PORT, ArenaConfig.DEFAULT_UDP_PORT, packet, eventBus);
  }

  public ArenaClient(String host, int tcpPort, int udpPort, Packet packet, EventBus eventBus) {
    this.host = host;
    this.tcpPort = tcpPort;
    this.udpPort = udpPort;
    this.client = new Client();
    packet.register(client);
    this.eventBus = eventBus;
    this.clientListener = new NetworkListener(eventBus);
    this.client.addListener(clientListener);
  }

  public void connect() throws ConnectException {
    try {
      this.client.start();
      this.client.connect(ArenaConfig.LATENCY, host, tcpPort, udpPort);
    } catch (IOException e) {
      throw new ConnectException(e.getMessage());
    }
  }

  public void disconnect() {
    this.client.stop();
  }

  public <P extends BasePacket<?>> void send(Channel channel, P packet) {
    if (channel == Channel.TCP) {
      client.sendTCP(packet);
    }
    if (channel == Channel.UDP) {
      client.sendUDP(packet);
    }
  }

  public <H> HandlerRegistration addHandler(Type<H> type, H handler) {
    return this.eventBus.addHandler(type, handler);
  }

  public int getTcpPort() {
    return tcpPort;
  }

  public int getUdpPort() {
    return udpPort;
  }

  public void clearHandlers() {
    
  }

}
