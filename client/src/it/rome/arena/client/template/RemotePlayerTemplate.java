package it.rome.arena.client.template;

import it.rome.arena.shared.Layers;
import it.rome.game.component.RenderableComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

public class RemotePlayerTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();

    entityEdit.add(new RenderableComponent(Layers.OTHERS));

  }

}
