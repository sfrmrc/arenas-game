package it.rome.arena.client.template;

import it.rome.arena.client.Images;
import it.rome.arena.shared.Layers;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.HealthComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.PointsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteAnimationComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.StateComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.attribute.PlayerState;
import it.rome.game.shared.attribute.Stats;
import it.rome.game.shared.component.FactionComponent;
import it.rome.game.shared.component.StatsComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.fsm.EntityStateMachine;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author m.sferra
 * @since 0.0.1
 * 
 */
public class PlayerTemplate extends EntityTemplate {

  private final BodyBuilder bodyBuilder;

  public PlayerTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String networkId = parameters.get("networkId");
    Spatial spatial = parameters.get("spatial");
    Stats stats = parameters.get("stats");

    if (networkId == null) {
      throw new IllegalArgumentException("NetworkId cannot be null");
    }

    if (spatial == null) {
      throw new IllegalArgumentException("Spatial parameter cannot be null");
    }

    if (spatial.getWidth() <= 0 || spatial.getHeight() <= 0) {
      throw new IllegalArgumentException("Illegal spatial parameter");
    }

    if (stats == null) {
      throw new IllegalArgumentException("Stats parameter cannot be null");
    }

    short faction = stats.getFaction();

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(spatial.getX(), spatial.getY())
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(spatial.getWidth() * 0.5f)
                    .maskBits(faction).categoryBits(faction).restitution(0f).friction(0f)
                    .density(1f)).build();

    Texture texture = Images.race(stats.getRace());
    Sprite sprite = new Sprite(texture);

    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    SpatialComponent spatialComponent = new SpatialComponent(new SpatialPhysicsImpl(body, spatial));
    SpriteComponent spriteComponent = new SpriteComponent(sprite);

    entityEdit.add(new VelocityComponent(0));
    entityEdit.add(spatialComponent);
    entityEdit.add(physicsComponent);
    entityEdit.add(new HealthComponent(stats.getHealth()));
    entityEdit.add(new PointsComponent(stats.getPoints()));
    entityEdit.add(new StatsComponent(stats));
    entityEdit.add(new FactionComponent(stats.getFaction()));

    entityEdit.add(spriteComponent);
    entityEdit.add(new RenderableComponent(Layers.PLAYER));
    entityEdit.add(createPlayerStates(entity, texture));

  }

  private StateComponent<PlayerState> createPlayerStates(Entity entity, Texture texture) {
    EntityStateMachine<PlayerState> esm = new EntityStateMachine<PlayerState>(entity);

    SpriteAnimationComponent nopeAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.3f, PlayMode.LOOP, 3, 4, 7, 1);
    esm.createState(PlayerState.NOPE).add(SpriteAnimationComponent.class)
        .withInstance(nopeAnimationComponent);

    SpriteAnimationComponent leftAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.3f, PlayMode.LOOP, 3, 4, 10, 3);
    esm.createState(PlayerState.W_LEFT).add(SpriteAnimationComponent.class)
        .withInstance(leftAnimationComponent);

    SpriteAnimationComponent rightAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.3f, PlayMode.LOOP, 3, 4, 4, 3);
    esm.createState(PlayerState.W_RIGHT).add(SpriteAnimationComponent.class)
        .withInstance(rightAnimationComponent);

    SpriteAnimationComponent upAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.3f, PlayMode.LOOP, 3, 4, 3);
    esm.createState(PlayerState.W_UP).add(SpriteAnimationComponent.class)
        .withInstance(upAnimationComponent);

    SpriteAnimationComponent downAnimationComponent =
        new SpriteAnimationComponent(texture, 0f, 0.3f, PlayMode.LOOP, 3, 4, 7, 3);
    esm.createState(PlayerState.W_DOWN).add(SpriteAnimationComponent.class)
        .withInstance(downAnimationComponent);

    esm.changeState(PlayerState.NOPE);
    StateComponent<PlayerState> stateComponent = new StateComponent<PlayerState>(esm);
    return stateComponent;
  }

}
