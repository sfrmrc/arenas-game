package it.rome.arena.client.template;

import it.rome.game.shared.component.InterpolationComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class InterpolatedTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    entityEdit.add(new InterpolationComponent());
  }

}
