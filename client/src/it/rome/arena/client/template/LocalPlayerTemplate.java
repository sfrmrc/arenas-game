package it.rome.arena.client.template;

import it.rome.arena.client.component.LocalNetworkComponent;
import it.rome.arena.client.component.PredictionComponent;
import it.rome.game.component.InputComponent;
import it.rome.game.component.PovComponent;
import it.rome.game.shared.attribute.Spell;
import it.rome.game.shared.component.WeaponComponent;
import it.rome.game.template.EntityTemplate;

import java.util.Collection;

import com.artemis.Entity;
import com.artemis.EntityEdit;

public class LocalPlayerTemplate extends EntityTemplate {

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    String networkId = parameters.get("networkId");
    Collection<Spell> spellBook = parameters.get("spellBook");

    if (networkId == null) {
      throw new IllegalArgumentException("NetworkId cannot be null");
    }

    entityEdit.add(new WeaponComponent(spellBook));
    entityEdit.add(new LocalNetworkComponent(networkId));
    entityEdit.add(new PredictionComponent());
    entityEdit.add(new InputComponent());
    entityEdit.add(new PovComponent());

  }

}
