package it.rome.arena.client.template;

import it.rome.arena.client.Images;
import it.rome.arena.shared.Layers;
import it.rome.game.attribute.EntityRef;
import it.rome.game.attribute.Spatial;
import it.rome.game.attribute.impl.SpatialImpl;
import it.rome.game.attribute.impl.SpatialPhysicsImpl;
import it.rome.game.builder.BodyBuilder;
import it.rome.game.component.DamageComponent;
import it.rome.game.component.PhysicsComponent;
import it.rome.game.component.RenderableComponent;
import it.rome.game.component.SpatialComponent;
import it.rome.game.component.SpriteComponent;
import it.rome.game.component.VelocityComponent;
import it.rome.game.shared.component.FactionComponent;
import it.rome.game.template.EntityTemplate;

import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * 
 * @author marco
 * @since 0.0.1
 *
 */
public class BulletTemplate extends EntityTemplate {

  private final BodyBuilder bodyBuilder;

  public BulletTemplate(BodyBuilder bodyBuilder) {
    this.bodyBuilder = bodyBuilder;
  }

  @Override
  public void apply(Entity entity) {
    EntityEdit entityEdit = entity.edit();
    EntityRef casterRef = parameters.get("casterRef");
    float x0 = parameters.get("x0");
    float y0 = parameters.get("y0");
    float x1 = parameters.get("x1", x0);
    float y1 = parameters.get("y1", y0);
    float speed = parameters.get("speed", 500f);
    int damage = parameters.get("damage", 100);
    int commandId = parameters.get("commandId", -1);

    if (speed == 0) {
      throw new IllegalArgumentException("Speed parameter cannot be null");
    }

    if (commandId == -1) {
      throw new IllegalArgumentException("CommandId parameter cannot be null");
    }

    float radius = 10f;

    FactionComponent factionComponent =
        casterRef.getEntity() != null
            ? casterRef.getEntity().getComponent(FactionComponent.class)
            : null;

    if (factionComponent == null) {
      throw new IllegalArgumentException("Caster entity must have Faction component");
    }

    short faction = factionComponent.getFaction();
    short mask = (short) ~faction;

    Body body =
        bodyBuilder
            .userData(entity)
            .type(BodyType.DynamicBody)
            .position(x0, y0)
            .fixture(
                bodyBuilder.fixtureDefBuilder().circleShape(radius * 0.3f).restitution(0f)
                    .density(0f).friction(0f).categoryBits(faction).maskBits(mask)).build();

    Spatial spatial = new SpatialImpl(x0, y0, radius, radius);
    PhysicsComponent physicsComponent = new PhysicsComponent(body);
    SpatialComponent spatialComponent = new SpatialComponent(new SpatialPhysicsImpl(body, spatial));


    Sprite sprite = new Sprite(Images.spell(commandId));

    Vector2 s = new Vector2(x1 - x0, y1 - y0);
    s.nor().scl(speed);

    entityEdit.add(new DamageComponent(damage));
    entityEdit.add(new RenderableComponent(Layers.SPELL));
    entityEdit.add(new VelocityComponent(s.x, s.y));
    entityEdit.add(new SpriteComponent(sprite));
    entityEdit.add(spatialComponent);
    entityEdit.add(physicsComponent);

  }

}
